c     library name : smmols.f   

c     Copyright (c) 2013 P.Arun Prasad, S.Nehru Viji and N.Gautham

c     This library is free software; you can redistribute it and/or
c     modify it under the terms of the GNU Lesser General Public
c     License as published by the Free Software Foundation; either
c     version 2.1 of the License, or (at your option) any later version.
c
c     This library is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
c     Lesser General Public License for more details.
c
c     You should have received a copy of the GNU Lesser General Public
c     License along with this library; if not, write to the Free
c     Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
c     02110-1301  USA
c
c
c     contact : n_gautham@hotmail.com  


c       program mols
	subroutine mols(inp,iopt)
	include 'mols.par'
	parameter(mxtyat = 18)
	common /ecpp/ aij(mxtyat,mxtyat),cij(mxtyat,mxtyat),
     &  a14(mxtyat,mxtyat),ihb(mxtyat,mxtyat),ahb(mxtyat,mxtyat),
     &  chb(mxtyat,mxtyat)
        common/parameters/e(maxord,maxord,maxpar),p(maxpar,maxord,3)
        common/crda/x(maxatm,8) 
        common/ranges/jstart(maxatm,10),jend(maxatm,10),j1_4(maxatm,12)
        common/crdb/y(maxatm,8) 
        common/energy/e_final(maxord,maxord) 
        common/mean/avrg1(maxpar,maxord)
        common/vectors/iv(maxpar,4)
        common /par/ natom, ntor, nhb, ns, lres
        common/hb/ihb1(maxhb),ihb2(maxhb),c(maxhb),d(maxhb)
        common/order/nn,mm
        common/freq/ifreq(maxpar,maxord,maxord)
        common/out/e_out(maxatm),p_out(maxpar,maxatm)
	common/mout/opmo(maxstr,maxpar),opmi(maxstr,maxpar),
     &              emo(maxstr),emi(maxstr)
        common/calls/ncalls
	common /scc/ sc(maxatm,maxpar,5),nrot(maxpar),nchi(maxpar)
	common/pdbat/atom(maxatm),ele(maxatm)
	common /tor/ u0(maxpar),sn(maxpar),tn(maxpar),phi(maxpar)       
	common /fnam/ if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4
	common /getrot/inrot,irotb(maxi,2),el,ilsrot(maxi,maxi),
     &  iatrot(maxi),rx(100,3),ire(maxi,maxi),ind(maxi,maxi),big,
     &  frotb(maxi,2)   
        common /left/ le(maxi,maxi),innd(maxi),ielenum(maxi),
     &  bonum(maxi)
	common /cen/bx,by,bz


	character*128 if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4,of0
c        common /res_i/ iatrs1(mxrs),iatrs2(mxrs),ixatrs(mxrs),ivrrs1(mxrs)
c    &  ,nvrrs(mxrs)
         
        dimension angl(maxpar,maxord),angle(maxpar,maxord),jseed(5000),
     & ang(maxpar,maxord)
	character seq*(maxres),fseq(maxatm)*1
	real plpe  !added for dock
	
c        nn=19 ! No. of torsion angles ( or search dimensions)
        mm=37 ! Size of MOLS dimensions
cn	print*,mm
c	natom=77
cc	write(31,*) 'Enter the no. of parameters'
cc	read *, nn
	nn = ntor
	npar = ntor + 6

c	print*,nn,npar
cc	write(31,*) 'Enter the no. of atoms'
cc	read *,natom
cc	write(31,*) 'How many optimal structures to be generated (1 to 2000)'
cc	read *,ns
cc	write(31,*) 'No. of Hydrogen bond pairs'
cc	read *,nhb

ccc	call input
ccc	call pdb
cccc	call flexall
        

	call precal !commented since no dock

	call anggen(ang,fseq,npar,iopt,aii)
	if(iopt.eq.3) call scinp(fseq)
        do i=1,natom
          do j=1,8
             y(i,j)=x(i,j)
          enddo          
	enddo
c	et=ecpene(1,1)
ccc	print *,'etot',et
ccc	stop
cccccccccc
ccc	return
ccccccccccc
c        open(unit=2,file='n37.inp',status='old')
        do i=1,npar
	  do j = 1,mm
          angl(i,j) =ang(i,j)
c	print*,angl(i,j)
	  enddo
        enddo

c275      format(37f6.1)
c         close(unit=2)
c************************************************************
c        open(unit=3,file='seed.inp',status='unknown')
	call rand1(jseed)
         nt=1
         do ijx=inp,inp!1,ns
	iseed = jseed(ijx)
c          read(3,*)iseed
c	write(31,*)iseed
          call write_par(angl,iseed,angle,npar)
          call pargen1(angle,npar)
        do i=1,100
          e_out(i)=1.0e+25
        enddo

        do i=1,npar
          do j=1,mm
            do k=1,mm
              ifreq(i,j,k)=0
            enddo
          enddo
        enddo
c         ************ Main MOLS ***************
         do l1=1,npar
         do l2=1,mm
            p(l1,l2,2)=0.0
            p(l1,l2,3)=0.0
          enddo
         enddo

          do i=1,mm
           do j=1,mm
	     k = 1
	
	     call subpar(i,j,k,iopt,fseq,aii,phi,npar)
	     call molgen(phi,1)
             rmmff=rmmffene(i,j)     
	     call eplp(plpe,hb,steric,1)
	    e_final(i,j)=rmmff
c	    print*,'PLP_subret',plpe
c	 stop
	e_final(i,j) = plpe + e_final(i,j) !energy along with plp
c	print *,i,j,'rmmffene=',rmmff,' PLP',plpe,'total=',e_final(i,j)


cv             	if(iff.eq.1) e_final(i,j)=ampene(i,j)
cv		if(iff.eq.2) e_final(i,j)=ecpene(i,j)
c	     call energee(i,j)
           enddo
          enddo          
          call average(npar)
          do i=1,npar
            call sort_and_set_rank(i)
          enddo
         do i=1,npar 
           call rank_sort(i,nt)
         enddo
c         ************ LAST MOLS *************
	do m1=1,15
          m2 = 2
	  kk = 1
	  call subpar(kk,m1,m2,iopt,fseq,aii,phi,npar)
c         print *,(phi(id),id=1,nn+6)
	  call molgen(phi,1)
c         print *,(phi(id),id=1,nn+6)
	  
cc
	rmmff=rmmffene(m1,m1)
	call eplp(plpe,hb,steric,1)
	e_final(m1,m1)=rmmff
	print *,'MOLS energy, PLP, final energies: ',e_final(m1,m1),
     &	plpe,e_final(m1,m1)+plpe

	e_final(m1,m1) = plpe+e_final(m1,m1) !energy along with plp

c	stop
cv	if(iff.eq.1)   e_final(m1,m1) = ampene(m1,m1)
cv	if(iff.eq.2)  e_final(m1,m1) = ecpene(m1,m1)
c	print *,m1,(phi(ih),ih=1,nn),e_final(m1,m1)
cc
c	call energee(m1,m1)
	  call best(m1,npar)
	enddo
        call output(1,ijx,et,iopt,fseq,aii,npar) !included npar in it
	write(*,*)'generated MOLS optimal Structure NO : ',ijx,et
c*******Flushing standard output************
	call flush(6)
c*******End of Flushing ********************
	write(31,*)'generated MOLS optimal Structure NO : ',ijx,et
333	enddo
c	stop
	return
        end

c********************************************************************
        subroutine write_par(angl,iseed,angle,npar)

	include 'mols.par'
        common/order/nn,mm
        dimension angll(maxpar,maxord),angle(maxpar,maxord),
     &  angl(maxpar,maxord)
c       open(unit=4,file='molspargen.inp',status='unknown')

        do i=1,npar
          do j=1,mm
            angll(i,j)=angl(i,j)
          enddo
        enddo
        amm=float(mm)
        do j=1,npar
         ki=0
         do i=1,100000
c         y=ran(iseed)
          y=rand(iseed)
          iseed = y
          iy=nint(y*amm)
          if(iy.eq.0)iy=1
           if (angll(j,iy).lt.999) then
            ki=ki+1
            angle(j,ki)=angll(j,iy)
            angll(j,iy)=99999.0
            if(ki.ge.mm)go to 3 
           endif         
          enddo         
3       continue
        enddo 

     
c        do i=1,npar
c         write(*,10)(angle(i,j),j=1,mm)
10       format(37f6.1)
c        enddo
    
c        close(unit=4)
        return 
        end
c*********************************************************************
	subroutine pargen1(angle,npar)

c	pargen is for parameter generation
c	n  -   total number of parameters
c	m  -   maximum number of values for each parameter
      
	include 'mols.par'
        common/parameters/e(maxord,maxord,maxpar),p(maxpar,maxord,3)
        common/order/nn,mm
	dimension angle(maxpar,maxord)
c	open (unit=5,file='molspargen.inp',status='unknown')

	do 150 i = 1, npar
	  do 150 j = 1,mm
	  p(i,j,1) = angle(i,j)
 150	continue
  10    format(37f6.1)
	do 500 l = 1, npar
	do 500 k = 1, mm
	do 500 j = 1, mm
	  i = mod( ((j-1) * (l-1) + (k-1)), mm) + 1
	  e(i,j,l)  = p(l,k,1)
c	write(31,*)i,'  ',j,'  ',l,'  ',e(i,j,l)

500	continue
        close (unit=5)
        
c20	format(8f6.1)
c	open (unit=6,file='pargen.out',status='unknown')
c	 write (6,20) (((e(i,j,l),l=1,npar),j=1,mm),i=1,mm)
c      close (unit=6)        

	return
	end

c*******************************************************************************************
 
        subroutine molgen(phi,tst)
c	program molgen(phi)
        include 'mols.par'
        common/parameters/e(maxord,maxord,maxpar),p(maxpar,maxord,3)
        common/crda/x(maxatm,8)
        common/crdb/y(maxatm,8)
        common /par/ natom, ntor, nhb, ns, lres
        common/vectors/iv(maxpar,4)
        common/order/nn,mm
        common /getrot/inrot,irotb(maxi,2),el,ilsrot(maxi,maxi),
     &   iatrot(maxi),rx(100,3),ire(maxi,maxi),ind(maxi,maxi),big
        common /left/ le(maxi,maxi),innd(maxi),ielenum(maxi),
     &  bonum(maxi)
	common /cen/bx,by,bz

c       integer le(maxi,maxi),elenum(maxi,maxi)

        dimension x_one(maxatm,3),x_two(maxatm,3)
        dimension phi(maxpar)
        integer tst,nn
        real rotang,theta,psi

        real cx,cy,cz,bx,by,bz
 
c	bx= -9.665
c	by= 16.179
c	bz= 1.860

	nn=ntor


c       write(31,*)(phi(ii),ii=1,nn)
c        write(*,*)(phi(ii),ii=1,nn)
        do k=1,natom
           do ki=1,3
             x_one(k,ki)=rx(k,ki)
c       write(*,*)x_one(k,ki)
           enddo
        enddo
        do if=1,nn

C###################### PHI ALL ####################################

        call elemen(x_one(irotb(if,1),1),x_one(irotb(if,1),2),
     &              x_one(irotb(if,1),3),
     &              x_one(irotb(if,2),1),x_one(irotb(if,2),2),
     &              x_one(irotb(if,2),3),
     &              el,em,en)


50      format(i4,1x,3f8.3)
        do j=1,ielenum(if) !ielenum = total number of ligand atoms
c       write(31,*)'j ',j,' ','if',if !if is the rotational bond number
c	print *,j,if,ielenum(if)
	k=le(if,j)
c	print *,if,j,k,ki,x_one(k,ki)
           do ki=1,3
           x_two(k,ki)=x_one(k,ki)
c	write(31,*)k,ki,x_two(k,ki)
c       write(*,50)k,x_two(k,ki)
           enddo
        enddo
c	write(31,*)'elemen smmols - done'
c       write(*,50)k,x_two(k,ki)
c       stop
c------------------------------------------------------
c        do k=1,iv(if,3)-1
c           do ki=1,3
c             x_two(k,ki)=x_one(k,ki)
c           enddo
c        enddo
c------------------------------------------------------
         do j=1,iatrot(if)
          k=ilsrot(if,j)
         
c        write(*,*)
           xin=x_one(k,1)-x_one(irotb(if,1),1)
           yin=x_one(k,2)-x_one(irotb(if,1),2)
           zin=x_one(k,3)-x_one(irotb(if,1),3)
           call rotor(el,em,en,phi(if),xin,yin,zin,
     &                xout,yout,zout)
c        print*,'phi',el,em,en,phi(if)
c	 write(31,*)'phi',el,em,en,phi(if)
c          write(*,*)'xoout',xout,yout,zout
           x_two(k,1)=xout+x_one(irotb(if,1),1)
           x_two(k,2)=yout+x_one(irotb(if,1),2)
           x_two(k,3)=zout+x_one(irotb(if,1),3)
        

c       print*,'rot_coord'
c       write(21,50),k,x_two(k,1),x_two(k,2),x_two(k,3)
c	write(31,50),k,x_two(k,1),x_two(k,2),x_two(k,3) !nothing got printed here
        enddo

        do k=1,natom
           do ki=1,3
              x_one(k,ki)=x_two(k,ki)
           enddo
        enddo
C####################################################################

        enddo

        do k=1,natom
           do ki=1,3
             y(k,ki)=x_two(k,ki)
c	 write(31,*)k,ki,y(k,ki)
c        write(61,50)k,y(k,ki)
           enddo
        enddo

	call rotate(bx,by,bz,phi(nn+4),phi(nn+5),phi(nn+6),natom)
	call translate(bx,by,bz,phi(nn+1),phi(nn+2),phi(nn+3),natom)

c	print*,phi(nn+1),phi(nn+2),phi(nn+3),natom
c	print*,phi(nn+4),phi(nn+5),phi(nn+6),natom
c        stop
        return
        end
c***********************************************************************
       function ampene(ie,je)
 
	include 'mols.par'
        common/crdb/y(maxatm,8) 
        common/ranges/jstart(maxatm,10),jend(maxatm,10),j1_4(maxatm,12)
        common/energy/e_final(maxord,maxord) 
        common /par/ natom, ntor, nhb, ns, lres
        common/hb/ihb1(maxhb),ihb2(maxhb),c(maxhb),d(maxhb)
        common/calls/ncalls
c	open (unit=7,file='energy.out',status='unknown')

        ees=0.0
        enb=0.0
        enb1_4=0.0
        ees1_4=0.0
        ehb=0.0
        etot=0.0

	do i=1,natom
          n_range=ifix(y(i,7))
         do ij=1,n_range
          k1=jstart(i,ij)
         k2=jend(i,ij)
          do j=k1,k2
          dis = dist(y(i,1),y(i,2),y(i,3),y(j,1),y(j,2),y(j,3))
            if(dis.lt.0.01) then
!              e_final(ie,je)=1.0e+25
	       ampene = 1.0e+25
              return
            endif
            call force(i,j,dis,enbt,eest)
            ees=ees+eest
            enb=enb+enbt
          enddo
         enddo

         n1_4=ifix(y(i,8))
         do ij=1,n1_4
            j=j1_4(i,ij)
          dis = dist(y(i,1),y(i,2),y(i,3),y(j,1),y(j,2),y(j,3))
            if(dis.lt.0.5) then
              STOP 'Input coordinates are wrong !!'
            endif
            call force(i,j,dis,enbt,eest)
            ees1_4=ees1_4+0.5*eest
            enb1_4=enb1_4+0.5*enbt
         enddo           
       enddo

        do i=1,nhb
         dis = dist( y(ihb1(i),1),y(ihb1(i),2),y(ihb1(i),3),
     &                  y(ihb2(i),1),y(ihb2(i),2),y(ihb2(i),3))
c     	 print *,y(ihb1(i),1),y(ihb1(i),2),y(ihb1(i),3),
c    &                  y(ihb2(i),1),y(ihb2(i),2),y(ihb2(i),3)	
         ess=dis*dis
         artwo=1.0/ess
         arten=artwo**5
         artwelve=arten*artwo
         ehb=ehb+c(i)*artwelve-d(i)*arten
         arstar=y(ihb1(i),5)+y(ihb2(i),5)
         epsilon=sqrt(y(ihb1(i),6)*y(ihb2(i),6))
         aaa=epsilon*(arstar**12)
         ccc=2.0*epsilon*(arstar**6)
         arsix=artwo*artwo*artwo
         ef1=aaa*artwelve
         ef2=ccc*arsix
         enbt=ef1-ef2
         enb=enb-enbt
        enddo
 


        ampene=enb+ees+ehb+enb1_4+ees1_4
c        etot=enb+ees+ehb+enb1_4+ees1_4
c         e_final(ie,je)=etot
c	write(7,*) e_final(ie,je)
c	write(31,*)e_final(ie,je)
c	close(unit=7)
c	write(*,*) e_final(ie,je),ie,je
        return
        end
c*******************************************************************
        subroutine force(if,jf,diss,enbt,eest)

	include 'mols.par'
        common/crdb/y(maxatm,8) 

        arstar=y(if,5)+y(jf,5)
        epsilon=sqrt(y(if,6)*y(jf,6))
        aaa=epsilon*(arstar**12)
        ccc=2.0*epsilon*(arstar**6)
        ess=diss*diss
        artwo=1.0/ess
        arsix=artwo*artwo*artwo
        artwelve=arsix*arsix
        eest=332.0*y(if,4)*y(jf,4)*artwo/4.0
        ef1=aaa*artwelve
        ef2=ccc*arsix
        enbt=ef1-ef2        
        return                  
        end

c***********************************************************************
        function ecpene(ie,je)
 
	include 'mols.par'
	parameter(mxtyat = 18)
        common/crdb/y(maxatm,8) 
        common/ranges/jstart(maxatm,10),jend(maxatm,10),j1_4(maxatm,12)
	common /tor/ u0(maxpar),sn(maxpar),tn(maxpar),phi(maxpar)
         common/energy/e_final(maxord,maxord) 
        common /par/ natom, ntor, nhb, ns, lres
        common/hb/ihb1(maxhb),ihb2(maxhb),c(maxhb),d(maxhb)
        common /ecpp/ aij(mxtyat,mxtyat),cij(mxtyat,mxtyat),
     &  a14(mxtyat,mxtyat),ihb(mxtyat,mxtyat),ahb(mxtyat,mxtyat),
     &  chb(mxtyat,mxtyat)
        common/calls/ncalls
c	open (unit=7,file='energy.out',status='unknown')
	
	cdc=(22.0/(7.0*180))
        ees=0.0
        enb=0.0
        ehb=0.0
	etor=0.0
        enb14=0.0
        ees14=0.0
	ehb14=0.0
        etot=0.0
c	do i = 1,natom
c	print *,i,y(i,1),y(i,2),y(i,3)
c	enddo
c
 	do in = 1,ntor
	etor = etor + u0(in)*(1.0+sn(in)*cos(cdc*((phi(in)-180.0))*tn(in)))
c	print *,in, etor , tn(in),u0(in),sn(in),phi(in)
c	write(31,*)in, etor , tn(in),u0(in),sn(in),phi(in)!nothing got printed here 
	enddo
c
	do i=1,natom
          n_range=ifix(y(i,7))
         do ij=1,n_range
          k1=jstart(i,ij)
         k2=jend(i,ij)
          do j=k1,k2
          dis = dist(y(i,1),y(i,2),y(i,3),y(j,1),y(j,2),y(j,3))
            if(dis.lt.0.01) then
!              e_final(ie,je)=1.0e+25
	       ecpene = 1.0e+25
              return
            endif
c
	ity = int(y(i,5))
	jty = int(y(j,5))
        ees = ees + (332.0*y(i,4)*y(j,4))/(dis*2.0)
	if(ihb(ity,jty).eq.0) then
	enb = enb + (aij(ity,jty)/(dis**12.0))-(cij(ity,jty)/(dis**6.0))
c       print *,i,j,dis,aij(ity,jty),cij(ity,jty),enb
	else
         ehb=ehb+(ahb(ity,jty)/(dis**12.0))-(chb(ity,jty)/(dis**10.0))
	endif
          enddo        
         enddo
c
         n1_4=ifix(y(i,8))
         do ij=1,n1_4
            j=j1_4(i,ij)
          dis = dist(y(i,1),y(i,2),y(i,3),y(j,1),y(j,2),y(j,3))
            if(dis.lt.0.5) then
              STOP 'Input coordinates are wrong !!'
            endif
c
	ity = int(y(i,5))
	jty = int(y(j,5))
        ees14 = ees14 + (332.0*y(i,4)*y(j,4))/(dis*2.0)
	if(ihb(ity,jty).eq.0) then
	enb14=enb14+(a14(ity,jty)/(dis**12.0))-(cij(ity,jty)/(dis**6.0))
	else
       ehb14=ehb14+(ahb(ity,jty)/(dis**12.0))-(chb(ity,jty)/(dis**10.0))
	endif
         enddo           
       enddo

c
	ees = ees + ees14
	enb = enb + enb14
	ehb = ehb + ehb14
        ecpene=enb+ees+ehb+etor
c	print *, ecpene,ie,je
c	ecpene = etot
c	print *,'elene1_4 =',ees14
c	print *,'vwene1_4 =',enb14
c	print *,'hbene1_4 =',ehb14
c	print *,'elene =',ees
c	print *,'vwene =',enb
c	print *,'hbene =',ehb
c	print *,ees,enb,ehb,etor
c	print *,'etot = ',etot
c         e_final(ie,je)=etot
c	close(unit=7)
        return
        end
c***********************************************************************
c********************************************************************
        subroutine average(npar)

	include 'mols.par'
        common/parameters/e(maxord,maxord,maxpar),p(maxpar,maxord,3)
        common/energy/e_final(maxord,maxord) 
        common/out/e_out(maxatm),p_out(maxpar,maxatm)
        common/mean/avrg1(maxpar,maxord)
        common/order/nn,mm

        data en_k_t/1.98578e+03/
c	open (unit=8,file='average.out',status='unknown')

c       en_k_t is boltzmannconst*avogadronumber*temp(=300 degree K)
c	average is for finding the average 
c	e_final   -  energy values of the conformation 
c	cutoff    -  for omitting impossible conformations 
c	avrg1     -  average without cutoff
c	n  -   total number of parameters = 9
c	m  -   maximum number of values for each parameter = 37

c	compute using mols

	do 400 l = 1, npar
	  do 500 k = 1, mm
	    avrg1(l,k) = 0.0 
            summ   = 0.0
            summ_mm= 0.0
	    do 600 j = 1, mm
	       i = mod( ( (j-1) * (l-1) + (k-1) ), mm) + 1
               weight=exp(-1.0*(e_final(i,j)/(en_k_t*0.01)))
  	       summ = summ + e_final(i,j)*weight
               summ_mm=summ_mm+weight
600	    continue
	    avrg1(l,k) = summ/summ_mm
c	write(8,*)avrg1(l,k)
c	close(unit=8) 
500	  continue
400    continue

        return
        end
c*********************************************************************
      
        subroutine sort_and_set_rank(is1)

	include 'mols.par'
        common/parameters/e(maxord,maxord,maxpar),p(maxpar,maxord,3)
        common/mean/avrg1(maxpar,maxord)
        common/order/nn,mm
        common/freq/ifreq(maxpar,maxord,maxord)

        dimension rank(maxord),icc(maxord)
c	open (unit=9,file='srank1.out',status='unknown')
c	open (unit=10,file='srank2.out',status='unknown')
c	open (unit=11,file='srank3.out',status='unknown')
c	open (unit=12,file='srank4.out',status='unknown')

c	do j=1,mm
c	write (9,*),avrg1(is1,j),p(is1,j,1)
c       enddo
c	close(unit=9)
        do j=2,mm
          aa=avrg1(is1,j)
          bb=p(is1,j,1)
          do i=j-1,1,-1
            if(avrg1(is1,i).le.aa) go to 10
            avrg1(is1,i+1)=avrg1(is1,i)
            p(is1,i+1,1)=p(is1,i,1)
          enddo
          i=0
10        avrg1(is1,i+1)=aa
          p(is1,i+1,1)=bb
        enddo
c	do j=1,mm
c	write (10,*),avrg1(is1,j),p(is1,j,1)
c        enddo
c	close(unit=10)

        do i=1,mm
          rank(i)=float(i)
        enddo

        do j=2,mm
          aa=rank(j)
          bb=p(is1,j,1)
          do i=j-1,1,-1
            if(p(is1,i,1).le.bb) go to 20
            p(is1,i+1,1)=p(is1,i,1)
            rank(i+1)=rank(i)
          enddo
          i=0
20        rank(i+1)=aa
          p(is1,i+1,1)=bb
        enddo

c	do j=1,mm
c	write (11,*)rank(j),p(is1,j,1)
c       enddo
c	close(unit=11)

        do i=1,mm
          jxx=ifix(rank(i))
          ifreq(is1,i,jxx)=ifreq(is1,i,jxx)+1
          p(is1,i,2)=p(is1,i,2)+rank(i)
          p(is1,i,3)=p(is1,i,3)+rank(i)*rank(i)
        enddo


c	do j=1,mm
c	write (12,*) p(is1,j,1),p(is1,j,2),p(is1,j,3)
c        enddo
c	close(unit=12)

        return
        end 
c********************************************************************
       
        subroutine rank_sort(ir1,nt)

	include 'mols.par'
        common/parameters/e(maxord,maxord,maxpar),p(maxpar,maxord,3)
        common/freq/ifreq(maxpar,maxord,maxord)
        common/order/nn,mm

        dimension ee(maxord)
c	open (unit=13,file='rank1.out',status='unknown')
c	open (unit=14,file='rank2.out',status='unknown')
c	open (unit=15,file='rank3.out',status='unknown')
c	open (unit=16,file='rank4.out',status='unknown')

c       calculate the average and sd
c	do j=1,mm
c	write (13,*) p(ir1,j,1),p(ir1,j,2),p(ir1,j,3)
c	enddo
c	close(unit =13)

        do i=1,mm
          sum_x=p(ir1,i,2)
          sum_x2=p(ir1,i,3)
          ave=sum_x/nt
          sd=sqrt((sum_x2/nt)-((sum_x/nt)*(sum_x/nt)))
          p(ir1,i,2)=ave
          p(ir1,i,3)=sd
        enddo       
c	do j=1,mm
c	write (14,*) p(ir1,j,1),p(ir1,j,2),p(ir1,j,3)
c	enddo
c	close(unit =14)

       do j=2,mm
          bb=p(ir1,j,1)
          cc=p(ir1,j,2)
          dd=p(ir1,j,3)
          do j1=1,mm
            ee(j1)=ifreq(ir1,j,j1)
          enddo
          do i=j-1,1,-1
            if(p(ir1,i,2).le.cc) go to 10
            p(ir1,i+1,3)=p(ir1,i,3)
            p(ir1,i+1,2)=p(ir1,i,2)
            p(ir1,i+1,1)=p(ir1,i,1)
            do j1=1,mm
               ifreq(ir1,i+1,j1)=ifreq(ir1,i,j1)
            enddo
          enddo
          i=0
10        p(ir1,i+1,1)=bb
          p(ir1,i+1,2)=cc
          p(ir1,i+1,3)=dd
          do j1=1,mm
            ifreq(ir1,i+1,j1)=ee(j1)
          enddo
        enddo

c	do j=1,mm
c	write (15,*) p(ir1,j,1),p(ir1,j,2),p(ir1,j,3)
c	enddo
c	close(unit =15)

	do j=2,mm
	 bb=p(ir1,j,1)
	 cc=p(ir1,j,2)
	 dd=p(ir1,j,3)
	 do j1=1,mm
         ee(j1)=ifreq(ir1,j,j1)
	 enddo
	  do i=j-1,1,-1
	   if (p(ir1,i,2).ne.cc) goto 40
	   if (p(ir1,i,3).le.dd) goto 40
	   p(ir1,i+1,3)=p(ir1,i,3)
	   p(ir1,i+1,2)=p(ir1,i,2)
	   p(ir1,i+1,1)=p(ir1,i,1)
	   do j1=1,mm
	   ifreq(ir1,i+1,j1)=ifreq(ir1,i,j1)
	   enddo
	  enddo
	  i=0
  40 	  p(ir1,i+1,1)=bb
	  p(ir1,i+1,2)=cc
	  p(ir1,i+1,3)=dd
	  do j1=1,mm
	  ifreq(ir1,i+1,j1)=ee(j1)
	  enddo
	enddo

c	do j=1,mm
c	write (16,*) p(ir1,j,1),p(ir1,j,2),p(ir1,j,3)
c	enddo
c	close(unit =16)

        return
        end

c*********************************************************************
       subroutine output(nou,ijx,et,iopt,fseq,aii,npar)

	include 'mols.par'
        common/out/e_out(maxatm),p_out(maxpar,maxatm)
        common/order/nn,mm
        common /par/ natom, ntor, nhb, ns, lres
	common /scc/ sc(maxatm,maxpar,5),nrot(maxpar),nchi(maxpar)
	common/mout/opmo(maxstr,maxpar),opmi(maxstr,maxpar),
     &              emo(maxstr),emi(maxstr)
	common /fnam/ if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4
	character*128 if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4,of0
	character fseq(maxatm)*1 
	dimension pp(maxpar)


c       print *,'npar=',npar,'nn=',nn
c       print *,'aii=',aii
c       print *,'from output routine'
c       print *,(p_out(j,1),j=1,npar)


	i = nou
	if(iopt.eq.3) then  
	j = 1
	k = 1
	jh = 1
172	if(fseq(k).eq.'P') go to 193
	pp(j) = p_out(jh,i)
	j = j + 1
	jh = jh + 1
193	pp(j) = p_out(jh,i)
	j = j + 1
	jh = jh + 1
	if(nrot(k).ne.0.) then
	j1 = p_out(jh,i)
c	j1 = int((j1/10.0) + 1.0)
	j1 = int((j1/aii) + 1.0)
c	print *,j1,aii
	j1 = mod(j1,nrot(k))
	if(j1.eq.0) j1 = nrot(k)
	do j2 = 1, nchi(k)
		pp(j) = sc(k,j1,j2)
		j = j + 1
	enddo
	jh = jh + 1
	endif
	k = k + 1
	if(k.le.lres) go to 172	
	endif  

	open(unit=24,file=of1,status='unknown')
c          write(24,*)(p_out(j,i),ji=1,npar),e_out(i)
c           write(24,*)(p_out(j,i),j=1,npar),e_out(i)

	if(iopt.ne.3) then
	do k1=1,nn
	opmo(ijx,k1) = p_out(k1,i)
	enddo 
	emo(ijx) = e_out(i)
	do k1 = 1,nn
c	pp(k1) = pp(k1) - 180.0
        p_out(k1,i) = p_out(k1,i) - 180.0

	enddo
c************
        do k2 = nn+1,nn+3
        tt1 = p_out(k2,i)
        opmo(ijx,k2) = tt1
        if(tt1.eq.0.0) then
        tt2 = 0.0
        else
c       tt2 = (tt1/10.0)*.28
        tt2 = (tt1/10.0)*0.13888889
c       tt2 = (tt1/10.0)*0.55555556
        endif
c       tt2 = tt2-5.0
        tt2 = tt2-2.5
c       tt2 = tt2-10.0
        p_out(k2,i)=tt2
        enddo
        do k2=nn+4,nn+6
        opmo(ijx,k2) = p_out(k2,i)
        enddo
        p_out(nn+6,i)=p_out(nn+6,i)/2
c************
C           write(24,10) ijx,e_out(i),(pp(j),j=1,nn)
           write(24,*) ijx,e_out(i),(p_out(j,i),j=1,nn+6)
           write(*,*) ijx,e_out(i),(p_out(j,i),j=1,nn+6)
        else
	do k1=1,nn
	opmo(ijx,k1) = pp(k1)
	enddo
	emo(ijx) = e_out(i)
	do k1 = 1,nn
	pp(k1) = pp(k1) - 180.0
	enddo
        
C**********************
c       STORING THE TRANS AND ROT PARAM
        ix = 5
        iy = 1

281     tt1 = p_out(npar-ix,i)
c
        opmo(ijx,nn+iy) = tt1
c
        if(tt1.eq.0.0) then
        tt2 = 0.0
        else
c       tt2 = (tt1/10.0)*.28
        tt2 = (tt1/10.0)*0.13888889
c       tt2 = (tt1/10.0)*0.55555556
        endif
c       tt2 = tt2-5.0
        tt2 = tt2-2.5
c       tt2 = tt2-10.0
        pp(nn+iy) = tt2
c       opmo(ijx,nn+iy) = tt2
c       print *,ix,iy,npar-ix,nn+iy
        ix = ix-1
        iy = iy+1
        if(iy.le.3) go to 281
        ix = 2
        iy = 4
282     pp(nn+iy) = p_out(npar-ix,i)
        opmo(ijx,nn+iy) = p_out(npar-ix,i)
c       print *,ix,iy,npar-ix,nn+iy
        ix = ix-1
        iy = iy+1
        if(iy.le.6) go to 282


        pp(nn+6) = pp(nn+6)/2
c       opmo(ijx,nn+6) = opmo(ijx,nn+6)/2
C*****************
        write(24,*) ijx,e_out(i),(pp(j),j=1,nn+6)
c       print *,'from output pp: ',(pp(j),j=1,nn+6),emo(ijx)
c       print *,'from output opmo: ',(opmo(ijx,j),j=1,nn+6),emo(ijx)
c       stop
        endif 

c

cc      do k1=1,nn+6
c       do k1=1,nn+5
c       opmo(ijx,k1) = p_out(k1,i)
c       enddo
c       emo(ijx) = e_out(i)
cc   write(24,*)ijx,e_out(i),(p_out(j,i),j=1,nn)
cc	endif	
	et = e_out(i)
C10	format(i4,1x,f15.2,<nn>(1x,f6.1))
	if(ijx.eq.ns) close(unit=24)
        return
        end

c********************************************************************
c*********************************************************************
c***********************************************************************
	subroutine best(ib,npar)
	include 'mols.par'
        common/parameters/e(maxord,maxord,maxpar),p(maxpar,maxord,3)
        common/energy/e_final(maxord,maxord) 
        common/order/nn,mm
        common/out/e_out(maxatm),p_out(maxpar,maxatm)

        do i=1,25
          if(e_final(ib,ib).lt.e_out(i)) then            
            do j=25,i+1,-1
              e_out(j)=e_out(j-1)
              do k=1,npar
                p_out(k,j)=p_out(k,j-1)
              enddo
            enddo
            e_out(i)=e_final(ib,ib)
	    do l = 1,npar
	      p_out(l,i) = p(l,ib,1)
	    enddo
            go to 10
          endif
         enddo
10       continue
         return
         end
C###############################################################################
	subroutine rand1(jseed)
	integer a,b,c,d,e,r,jseed(5000)
c        open(unit=8,file='mseed.out',status='unknown')
	a=2346
	b=3465
	c=5421
	d=5323
	e=5000 
	  nss=0
           do i=1,10000
             r = mod(((a*b)+c),d)
	if (r.lt.0)then
	r = r * -1
	endif
             r=(r*1)*2+1
             if(r.le.999.or.r.gt.9999) go to 1
             nss=nss+1
             if(nss.gt.e)go to 2
c             write(8,*)r
	jseed(nss) = r	
1       b = r
	enddo

2       return
        end
c*************************************************************************
	subroutine anggen(ang,fseq,npar,iopt,aii)
	include 'mols.par'
        common/order/nn,mm
        common /par/ natom, ntor, nhb, ns, lres
	dimension ang(maxpar,maxord)
	character fseq(maxatm)*1 
	
c	open (unit=21,file='angles.inp',status='unknown')
c	type *,'enter nn (total no. of parameters)'
c	read *,nn
c	type *,'enter mm ((37)maximum no. of values for each parameters)'
c	read *,mm

  10    format(37f6.1)
cc        type *,'enter the interval to generate input angle values'
cc        read *,ii
cccc	ii = 10
cc        type *,'enter the starting value to generate input angle values'
cc	read *,ll

	if(iopt.eq.3) then
		npar = lres*3
		do i = 1,lres
		if(fseq(i).eq.'A'.or.fseq(i).eq.'G'.or.fseq(i).eq.'P') then
		npar = npar - 1
		endif
		enddo
	 npar = npar + 6
	endif
cc-------------------------------------------------------------------
cc	check the mols order it should be greater than npar and prime
cc	number
c	print *,mm,npar
	if(mm.lt.npar) then
	do i1 = 1,100
	k1 = 2
181	if(i1/k1*k1.eq.i1) go to 191
	k1 = k1 + 1
	if(i1.le.i1/2) go to 181
	if(i1.gt.npar) go to 201
c	print *,i1
191	enddo
201	mm = i1
	i1 = 0
	endif
c	print *,mm,npar
cc-------------------------------------------------------------------
	aii = 360.0/(mm-1)
c	print *,'o.k',aii
	all = 0
	do i=1,npar
	ang(i,1)=all
        b=all
	do j=2,mm
c        write(21,10)ang(i,j)
	ang(i,j)=b+aii
        b=ang(i,j)
        enddo
	enddo
c        write(21,10)((ang(i,j),j=1,mm),i=1,npar)
c       write(6,10)((ang(i,j),j=1,mm),i=1,npar)
c	close(unit=21)
	return 
	end
c*************************************************************************
	subroutine scinp(fseq)
	include 'mols.par'
        common /par/ natom, ntor, nhb, ns, lres
	character fseq(maxatm)*1,ch*1, tcode*4, scode*1
	common /scc/ sc(maxatm,maxpar,5),nrot(maxpar),nchi(maxpar)
cc	dimension sc(maxatm,maxpar,5),nrot(maxpar),nchi(maxpar)
20	format(a4,a1,1x,i2,i2)
21	format(20x,5f7.1)
11	format(a18,i3)

c	write(6,*) 'The sequence   : ',(fseq(i),i=1,lres)

 	open(unit=33,file='SC.lib',status='old')
	do i = 1,lres
	rewind 33
	do i1 = 1,234
	read(33,20) tcode, scode, mrot, mchi
c	write(31,*)tcode, scode, mrot, mchi
	if(scode.eq.fseq(i).and.mrot.ne.0) then
	  do i2 = 1, mrot
	  read(33,21) (sc(i,i2,i3),i3=1,mchi)
c	write(31,*)(sc(i,i2,i3),i3=1,mchi) !this subroutine is not executed
	  enddo
	nrot(i) = mrot
	nchi(i) = mchi
	go to 212
	endif
	enddo
212	enddo
	do i = 1,lres
	do j = 1, nrot(i)
c	write(31,*)(sc(i,j,k),k=1,nchi(i))
	enddo
	enddo
	close(unit=33)
	return
	end
	
c************************************************************************	
	subroutine subpar(l1,l2,l3,iopt,fseq,aii,phi,npar)
	include 'mols.par'
        common/parameters/e(maxord,maxord,maxpar),p(maxpar,maxord,3)
        common/crda/x(maxatm,8) 
        common/crdb/y(maxatm,8) 
        common /par/ natom, ntor, nhb, ns, lres
        common/vectors/iv(maxpar,4)
        common/order/nn,mm
	common /scc/ sc(maxatm,maxpar,5),nrot(maxpar),nchi(maxpar)
 
	character fseq(maxatm)*1 
c	dimension sc(1500,50,5),nrot(50),nchi(5)
        dimension x_one(maxatm,3),x_two(maxatm,3)
        dimension phi(maxpar)
	
c	write(31,*)iopt
	if(iopt.eq.3) then   ! this portion is skipped as iopt !=3
	i = 1
	j = 1
	k = 1
161	if(l3.eq.1) then  
	if(fseq(k).eq.'P') go to 191 
	phi(i) = e(l1,l2,j)
	i = i + 1
	j = j + 1
191	phi(i) = e(l1,l2,j)
	i = i + 1
	j = j + 1
	if(nrot(k).ne.0.) then
	j1 = e(l1,l2,j)
c	j1 = int((j1/10.0) + 1.0)
	j1 = int((j1/aii) + 1.0)
	j1 = mod(j1,nrot(k))
	if(j1.eq.0) j1 = nrot(k)
c	write(31,*)j1,k,nrot(k),nchi(k)
c	write(*,*)j1,k,nrot(k),nchi(k)
	do j2 = 1, nchi(k)
		phi(i) = sc(k,j1,j2)
		i = i + 1
	enddo
	j = j + 1
	endif
	k = k + 1
	endif

	if(l3.eq.2) then
	if(fseq(k).eq.'P') go to 192
	phi(i) = p(j,l2,1)
	i = i + 1
	j = j + 1
192	phi(i) = p(j,l2,1)
	i = i + 1
	j = j + 1
	if(nrot(k).ne.0.) then
	j1 = p(j,l2,1)
c	j1 = int((j1/10.0) + 1.0)
	j1 = int((j1/aii) + 1.0)
	j1 = mod(j1,nrot(k))
	if(j1.eq.0) j1 = nrot(k)
	do j2 = 1, nchi(k)
		phi(i) = sc(k,j1,j2)
		i = i + 1
	enddo
	j = j + 1
	endif
	k = k + 1
	endif

	if(k.le.lres) go to 161
	else
	do i = 1, nn
	if(l3.eq.1) phi(i) = e(l1,l2,i)
	if(l3.eq.2) phi(i) = p(i,l2,1)
	enddo
	endif

c***modified to include rotation and translation parameters for ligand docking**************
c       do i = nn+1, nn+3
c       if(l3.eq.1) tt1 = e(l1,l2,i)
c       if(l3.eq.2) tt1 = p(i,l2,1)
c       phi(i)=tt1
c       enddo
c       do i = nn+4, nn+6
c       if(l3.eq.1) phi(i) = e(l1,l2,i)
c       if(l3.eq.2) phi(i) = p(i,l2,1)
c       enddo
c       write(*,*)(phi(ii),ii=1,nn+6)

        do i = nn+1, nn+3
        if(l3.eq.1) tt1 = e(l1,l2,i)
        if(l3.eq.2) tt1 = p(i,l2,1)
        if(tt1.eq.0.0) then
        tt2 = 0.0
        else
c       tt2 = (tt1/10.0)*.28
        tt2 = (tt1/10.0)*0.13888889
c       tt2 = (tt1/10.0)*0.55555556
        endif
c       tt2 = tt2-5.0
        tt2 = tt2-2.5
c       tt2 = tt2-10.0
        phi(i) = tt2
        enddo
        do i = nn+4, nn+6
        if(l3.eq.1) phi(i) = e(l1,l2,i)
        if(l3.eq.2) phi(i) = p(i,l2,1)
        enddo

        if(iopt.eq.3) then
c       print *,nn,npar
c       change ix according to the no. of parameters added for trans and rot
c       ix = 5
c       iy = 1
        ix = 5
        iy = 1

c181    tt1 = e(l1,l2,npar-ix)
181     if(l3.eq.1) tt1 = e(l1,l2,npar-ix)
        if(l3.eq.2) tt1 = p(npar-ix,l2,1)
        if(tt1.eq.0.0) then
        tt2 = 0.0
        else
c       tt2 = (tt1/10.0)*.28
        tt2 = (tt1/10.0)*0.13888889
c       tt2 = (tt1/10.0)*0.55555556
        endif
c       tt2 = tt2-5.0
        tt2 = tt2-2.5
c       tt2 = tt2-10.0
        phi(nn+iy) = tt2
c       print *,ix,iy,npar-ix,nn+iy
        ix = ix-1
        iy = iy+1
        if(iy.le.3) go to 181
        ix = 2
        iy = 4
c182    phi(nn+iy) = e(l1,l2,npar-ix)
182     if(l3.eq.1) phi(nn+iy) = e(l1,l2,npar-ix)
        if(l3.eq.2) phi(nn+iy) = p(npar-ix,l2,1)
c       print *,ix,iy,npar-ix,nn+iy
        ix = ix-1
        iy = iy+1
c       if(iy.le.6) go to 182
        if(iy.le.6) go to 182
        endif 
c       print *,'npar =',npar,'nn =',nn
c       write(*,*)(phi(ii),ii=1,nn+6)
c	stop

c********************************************************

c       write(31,*)(phi(ii),ii=1,nn)
c       write(*,*)(phi(ii),ii=1,nn)
c       write(*,*)(phi(ii),ii=1,nn+6)
c       write(*,*)(phi(ii),ii=1,nn+5)
c       stop
 	return
	
	end
c***********************************************************************

c**************Calculate ligand centroid*****************************
        subroutine calcent(natl,cx,cy,cz)
        include 'mols.par'
        common/crdb/y(maxatm,8)
        integer natl
        real cx,cy,cz,cx1,cy1,cz1
c       print *,cx,' ',cy,' ',cz
c       print *,natl
c       print *,'START OF LIGATOM'
        cx=0.0
        cy=0.0
        cz=0.0

c       do i = 1,natl 
c          write (6,FMT='(f8.3,2x,f8.3,2x,f8.3)')y(i,1),y(i,2),y(i,3)
c       enddo

        do i = 1,natl
                cx = cx + y(i,1)
                cy = cy + y(i,2)
                cz = cz + y(i,3)
c         write (6,FMT='(f8.3,2x,f8.3,2x,f8.3)')y(i,1),y(i,2),y(i,3)
        enddo 
        cx1 = cx / natl
        cy1 = cy / natl
        cz1 = cz / natl
        cx = cx1
        cy = cy1
        cz = cz1
c       print *,cx,' ',cy,' ',cz
c       stop
c       print *,'Center of mass'
c       write(6,FMT='(f10.5,2x,f10.5,2x,f10.5)')cx,cy,cz
        return
        end
c**************End of ligand centroid******************************
c*****Translate ligand inside the grid as guided by MOLS******

        subroutine translate(bx,by,bz,tx,ty,tz,natl)
        include 'mols.par'
        common/crdb/y(maxatm,8)
        integer natl
        real cx,cy,cz,dx,dy,dz
c       print *,'translation parameters'
c       print *,bx,'  ',by,'  ',bz,'  ',tx,'  ',ty,'  ',tz

c       call calcent(natl,cx,cy,cz)
c       print *,'LIGAND CENTROID'
c       print *,cx,' ',cy,' ',cz

c       print *,'BEFORE TRANSLATION'
c       do i = 1,natl
c       write(6,FMT='(f8.3,2x,f8.3,2x,f8.3)')y(i,1),y(i,2),y(i,3)
c       enddo

c       print *,natl

c*******USED FOR MOVING CENTROID AGAIN TO ORGIN******
        call calcent(natl,cx,cy,cz)
        do i = 1,natl
                y(i,1) = cx-y(i,1)
                y(i,2) = cy-y(i,2)
                y(i,3) = cz-y(i,3)
        enddo
c       print *,'centroid after rotation: ',cx,cy,cz
c*******USED FOR MOVING CENTROID AGAIN TO ORGIN******


        do i = 1,natl
                y(i,1) = y(i,1)+(bx)+tx
                y(i,2) = y(i,2)+(by)+ty
                y(i,3) = y(i,3)+(bz)+tz
        enddo


c       print *,'AFTER TRANSLATION'
c       do i = 1,natl
c       write(6,FMT='(f8.3,2x,f8.3,2x,f8.3)')y(i,1),y(i,2),y(i,3)
c       enddo
c       stop
        return
        end
c*************End of Translation****************************

c*****Rotate ligand by angles Rx, Ry and Rz about****
c*****       x, y and z axiz respectively        ****

c       subroutine rotate(rx,ry,rz,natl)
        subroutine rotate(bx,by,bz,rotrang,theta,psi,natl)
        include 'mols.par'
        common/crdb/y(maxatm,8)
        real cx,cy,cz,theta1,psi1,rotrang1
        integer natl,r
        real a,b,c,d,e,f,ad,bd,rmat(3,3)
        real w1,w2,qx,qy,qz,tw,tx,ty,tz,tw1,tx1
        real ax,ay,az,sx,sy,sz,s,t
10      format(a6,2x,i5,a10,i5,4x,3f8.3)
	
c	print*,'bx=',bx
c	print*,'by=',by
c	print*,'bz=',bz
        rotrang1 = rotrang
        theta1 = theta
        psi1 = psi
c	print*,psi1
c	print*,rotrang1
c	print*,theta1
c       print *, 'rotation angles:',rx,ry,rz
        theta1=theta1*3.1415927/180.0
c       psi angle range {0<=psi<=180}
        psi1=(psi1*3.1415927/180.0)/2
c       rz=rz*3.1415927/180.0
c       print *,'rotang:',rotrang
c       CONVERT DEGREES TO RADIAN
        rotrang1=rotrang1*3.1415927/180.0
c       print *,'rotrang(radian):',rotrang

        r = 1
        w2 = 0.0
        sx = 0.0
        sy = 0.0 
        sz = 0.0
c       CREATE POINTS ON THE UNIT SPHERE USING SPHERICAL COORD

        sx = r*cos(theta1)*sin(psi1)
        sy = r*sin(theta1)*sin(psi1)
        sz = r*cos(psi1) 

c       MOVE THE SPHERE CENTRE TO THE LIGAND CENTROID
c       print *,'sphere points',sx,sy,sz
c       sx = sx + bx
c       sy = sy + by
c       sz = sz + bz


c       vdis = dist(sx,sy,sz,bx,by,bz)
        vdis = dist(sx,sy,sz,0.0,0.0,0.0)
c        print *,'radius:',vdis
c       print *,'sphere points',sx,sy,sz
c       write(16,10) 'HETATM',r,"  O   HOH ",r,sx,sy,sz
c       print *,'ligand centroid',bx,by,bz

c       CREATE THE AXIS OF ROTATION

c       ax = (sx - bx)/vdis
c       ay = (sy - by)/vdis
c       az = (sz - bz)/vdis

        ax = sx/vdis
        ay = sy/vdis
        az = sz/vdis

c       print *,'axis of rot:',ax,ay,az

c***************QUATERNION ROTATION******************

c       CREATE UNIT QUATERNION FOR ROTATION

c       w1 = cos(rotrang/2)
c       qx = ax*sin(rotrang/2)
c       qy = ay*sin(rotrang/2)
c       qz = az*sin(rotrang/2)
c
c       print *,'quat mag:',sqrt(w1*w1+qx*qx+qy*qy+qz*qz)
cc      stop

c       do i = 1,natl
c
cc      QUATERNION MULTIPLICATION - TAKING ONLY VECTOR PART
cc      unit quat X coordinate points

c       tw = w1*w2-(qx*y(i,1)+qy*y(i,2)+qz*y(i,3))
c       tx = qx*w1+w2*y(i,1)+qy*y(i,3)-qz*y(i,2)
c       ty = w1*y(i,2)-qx*y(i,3)+qy*w2+qz*y(i,1)
c       tz = w1*y(i,3)+qx*y(i,2)-qy*y(i,1)+qz*w2
c
cc      create q inverse
c       qx = -qx
c       qy = -qy
c       qz = -qz
c
cc      unit quat X coordinate points X inv(unit_quat)
c       tw1 = tw*w1-(tx*qx+ty*qy+tz*qz)
c       tx1 = tx*w1+tw*qx+ty*qz-tz*qy
c       ty1 = tw*qy-tx*qz+ty*w1+tz*qx
c       tz1 = tw*qz+tx*qy-ty*qx+tz*w1
c
c       y(i,1) = tx1
c       y(i,2) = ty1
c       y(i,3) = tz1
cc      print *,tx1,ty1,tz1
c
cc      enddo
cc      stop
c*********END OF QUATERNION ROTATION**************

c*********ANGLE/AXIS ROTATION******************
c       print *,'Before rotation:'
c       do i=1,natl
c       print *,y(i,1),y(i,2),y(i,3)
c       enddo
c       print *,'After rotation:'
        c = cos(rotrang1)
        s = sin(rotrang1)
        t = 1-cos(rotrang1)
c       ROTATION MATRIX
        rmat(1,1) = t*ax*ax+c
        rmat(1,2) = t*ax*ay+s*az
        rmat(1,3) = t*ax*az-s*ay
        rmat(2,1) = t*ax*ay-s*az
        rmat(2,2) = t*ay*ay+c
        rmat(2,3) = t*ay*az+s*ax
        rmat(3,1) = t*ax*az+s*ay
        rmat(3,2) = t*ay*az-s*ax
        rmat(3,3) = t*az*az+c
c       print *,'rmat:',rmat(1,1),rmat(1,2),rmat(1,3)
c       print *,'rmat:',rmat(2,1),rmat(2,2),rmat(2,3)
c       print *,'rmat:',rmat(3,1),rmat(3,2),rmat(3,3)

        call calcent(natl,cx,cy,cz)
        do i = 1,natl
                y(i,1) = cx-y(i,1)
                y(i,2) = cy-y(i,2)
                y(i,3) = cz-y(i,3)
        enddo
c       print *,'centroid before moving: ',cx,cy,cz
c       call calcent(natl,cx,cy,cz)
c       print *,'centroid after moving: ',cx,cy,cz
        do k=1,natl
        tx=rmat(1,1)*y(k,1)+rmat(1,2)*y(k,2)+rmat(1,3)*y(k,3)
        ty=rmat(2,1)*y(k,1)+rmat(2,2)*y(k,2)+rmat(2,3)*y(k,3)
        tz=rmat(3,1)*y(k,1)+rmat(3,2)*y(k,2)+rmat(3,3)*y(k,3)
        y(k,1) = tx
        y(k,2) = ty
        y(k,3) = tz
c       print *,tx,ty,tz
        enddo


c       do i=1,natl
c       print *,y(i,1),y(i,2),y(i,3)
c       enddo
c       stop

c***********END OF ANGLE/AXIS REPRESENTATION**********

c************EULER ANGLE ROTATION*********************
c       print *, 'rotation angles:',rx,ry,rz
c       a = cos(rx)
c       b = sin(rx) 
c       c = cos(ry) 
c       d = sin(ry) 
c       e = cos(rz) 
c       f = sin(rz) 

ccc     Rotation matrix in zxz convention

c       rmat(1,1) = (e*a)-(c*b*f)
c       rmat(1,2) = (e*b)+(c*a*f)
c       rmat(1,3) = f*d
c       rmat(2,1) = -(f*a+c*b*e)
c       rmat(2,2) = -f*b+c*a*e
c       rmat(2,3) = e*d
c       rmat(3,1) = d*b
c       rmat(3,2) = -d*a 
c       rmat(3,3) = c
ccc     Transpose
cc      rmat(1,1) = (e*a)-(c*b*f)
cc      rmat(1,2) = -(f*a+c*b*e)
cc      rmat(1,3) = d*b
cc      rmat(2,1) = (e*b)+(c*a*f)
cc      rmat(2,2) = -f*b+c*a*e
cc      rmat(2,3) = -d*a
cc      rmat(3,1) = f*d
cc      rmat(3,2) = e*d
cc      rmat(3,3) = c


c       call calcent(natl,cx,cy,cz)
cc      write(6,FMT='(f8.3,2x,f8.3,2x,f8.3)')cx,cy,cz

cc      MOVE THE CENTRE OF MASS TO THE ORIGIN

cc      print *,'BEFORE'
cc      do i = 1,natl
cc      write(6,FMT='(f8.3,2x,f8.3,2x,f8.3)')y(i,1),y(i,2),y(i,3)
cc      enddo

cc      call conformation(0)

c       do i = 1,natl
c               y(i,1) = cx-y(i,1)
c               y(i,2) = cy-y(i,2)
c               y(i,3) = cz-y(i,3)
c       enddo

cc      APPLY ROTATION TRANSFORMATION ON COORDINATES

c       do i = 1,natl
c       tx=rmat(1,1)*y(i,1)+rmat(1,2)*y(i,2)+rmat(1,3)*y(i,3)
c       ty=rmat(2,1)*y(i,1)+rmat(2,2)*y(i,2)+rmat(2,3)*y(i,3)
c       tz=rmat(3,1)*y(i,1)+rmat(3,2)*y(i,2)+rmat(3,3)*y(i,3)
c       y(i,1) = tx
c       y(i,2) = ty
c       y(i,3) = tz
c       enddo
cc      call conformation(0)
c       do i = 1,natl
c               y(i,1) = cx + y(i,1)
c               y(i,2) = cy + y(i,2)
c               y(i,3) = cz + y(i,3)
c       enddo
cc      call conformation(0)
cc      print *,'AFTER ROTATION'
cc      do i = 1,natl
cc      write(6,FMT='(f8.3,2x,f8.3,2x,f8.3)')y(i,1),y(i,2),y(i,3)
cc      enddo
cc      stop
c************END OF EULER ANGLE ROTATION*********************



        return
        end


c*************Precalculate the interaction pair***********
        subroutine precal
        include 'mols.par'
        common /preset/ hbp(500000),hbl(500000),sp(500000),sl(500000),
     &          iscnt,ihcnt
        common /plp/ pat(mnatp), lat(maxatm),patnam(mnatp),
     & presnam(mnatp),pchid(mnatp),
     &  tpres(25), tatnam(25,50), natp,
     & tatyp(25,50), ntatp(25), px(mnatp,3),hb,steric
        common /par/ natom, ntor, nhb, ns, lres
        character xatnam*4, xresnam*3,lat*2,pat*2,tatyp*2
        character patnam*4,presnam*3,tpres*3,tatnam*4
        character ltyp*4,ptyp*4

c       print *,'HB SET'
        ik = 1
        do i = 1,natp
        ptyp = pat(i)
        do j = 1,natom
        ltyp = lat(j)
      if((ptyp.eq.'D'.and.ltyp.eq.'A').or.
     & (ptyp.eq.'D'.and.ltyp.eq.'DA').or.
     & (ptyp.eq.'A'.and.ltyp.eq.'D').or.
     & (ptyp.eq.'A'.and.ltyp.eq.'DA').or.
     & (ptyp.eq.'DA'.and.ltyp.eq.'D').or.
     & (ptyp.eq.'DA'.and.ltyp.eq.'A').or.
     & (ptyp.eq.'DA'.and.ltyp.eq.'DA')) then
        hbp(ik) = i
        hbl(ik) = j
c       print *,hbp(ik),hbl(ik),patnam(i)
        ik = ik + 1
        endif
        enddo
        enddo
c       stop
        ihcnt = ik-1

c       print *,'STERIC SET'
        ik = 1 
c       print *,natp,natom
        do i = 1,natp
        ptyp = pat(i)
        do j = 1,natom
        ltyp = lat(j)
      if((ptyp.eq.'D'.and.ltyp.eq.'D').or.
     & (ptyp.eq.'D'.and.ltyp.eq.'NP').or.
     & (ptyp.eq.'A'.and.ltyp.eq.'A').or.
     & (ptyp.eq.'A'.and.ltyp.eq.'NP').or.
     & (ptyp.eq.'DA'.and.ltyp.eq.'NP').or.
     & (ptyp.eq.'NP'.and.ltyp.eq.'A').or.
     & (ptyp.eq.'NP'.and.ltyp.eq.'D').or.
     & (ptyp.eq.'NP'.and.ltyp.eq.'DA').or.
     & (ptyp.eq.'NP'.and.ltyp.eq.'NP')) then
        sp(ik) = i
        sl(ik) = j
c       print *,sp(ik),sl(ik),patnam(i),presnam(i)
c       print *,ptyp,ltyp
c	write(31,*)sp(ik),sl(ik),patnam(i),presnam(i)
c	write(31,*)ptyp,ltyp

        ik = ik + 1
        endif
        enddo
        enddo
        iscnt = ik-1

        return
c        stop
        end
c*************End pf Precalculation***********

c**************PLP Energy calculation**************
        subroutine eplp(plpe,hb,steric,tst)
        include 'mols.par'
        common/crdb/y(maxatm,8)
        common /par/ natom, ntor, nhb, ns, lres
        common /preset/ hbp(500000),hbl(500000),sp(500000),sl(500000),
     &          iscnt,ihcnt
        common /plp/ pat(mnatp), lat(maxatm),patnam(mnatp),
     & presnam(mnatp),pchid(mnatp),
     &  tpres(25), tatnam(25,50), natp,
     & tatyp(25,50), ntatp(25), px(mnatp,3)
        character xatnam*4, xresnam*3,lat*2,pat*2,tatyp*2
        character patnam*4,presnam*3,tpres*3,tatnam*4
        character ltyp*4,ptyp*4
c       integer hbp(90000),hbl(90000),sp(90000),sl(90000)
        real d,a,b,c,y1,ene,plpe,hb,steric
        integer tst
        d = 0.0
        a = 0.0
        b = 0.0
        c = 0.0
        ene = 0.0
        hb = 0.0
        steric = 0.0
c       do i = 1,natp
c       ptyp = pat(i)
c       do j = 1,natom
c       ltyp = lat(j)
c     if((ptyp.eq.'D'.and.ltyp.eq.'A').or.
c     & (ptyp.eq.'D'.and.ltyp.eq.'DA').or.
c    & (ptyp.eq.'A'.and.ltyp.eq.'D').or.
c     & (ptyp.eq.'A'.and.ltyp.eq.'DA').or.
c     & (ptyp.eq.'DA'.and.ltyp.eq.'D').or.
c     & (ptyp.eq.'DA'.and.ltyp.eq.'A').or.
c     & (ptyp.eq.'DA'.and.ltyp.eq.'DA')) then
cc      im = im + 1
c       d = dist(px(i,1),px(i,2),px(i,3),y(j,1),y(j,2),y(j,3))
cc      print *,ptyp,'  ',ltyp,'  ',d
c       if(d.le.3.4) then
c	if(d.le.2.3.and.d.ge.0) then
c        a = 20.0
c        b = 2.3
c        c = 46.0
c        else 
c         if(d.le.2.6.and.d.ge.2.3) then
c         a = 2.0
c         b = 0.3
c         c = 4.6
c         else
c          if(d.le.3.1.and.d.ge.2.6) then
c          a = 0.0 
c          b = 1.0 
c          c = -2.0
c         else
c         if(d.le.3.4.and.d.ge.3.1) then
c           a = 2.0
c           b = -0.3
c           c = 6.8
c           endif
c          endif
c        endif
c       endif
c       y1 = (c-(a*d))/b
c       ene = ene + y1
c      endif
c       endif
c
c      if((ptyp.eq.'D'.and.ltyp.eq.'D').or.
c     & (ptyp.eq.'D'.and.ltyp.eq.'NP').or.
c     & (ptyp.eq.'A'.and.ltyp.eq.'A').or.
c     & (ptyp.eq.'A'.and.ltyp.eq.'NP').or.
c     & (ptyp.eq.'DA'.and.ltyp.eq.'NP').or.
c     & (ptyp.eq.'NP'.and.ltyp.eq.'A').or.
c     & (ptyp.eq.'NP'.and.ltyp.eq.'D').or.
c     & (ptyp.eq.'NP'.and.ltyp.eq.'DA').or.
c     & (ptyp.eq.'NP'.and.ltyp.eq.'NP')) then
cc      im = im + 1
c       d = dist(px(i,1),px(i,2),px(i,3),y(j,1),y(j,2),y(j,3))
cc      print *,ptyp,'  ',ltyp,'  ',d
c       if(d.le.5.5) then
c        if(d.le.3.4.and.d.ge.0) then
c        a = 20.0
c        b = 3.4
c        c = 68.0
c        else
c         if(d.le.3.6.and.d.ge.3.4) then
c         a = 0.4
c         b = 0.2
c         c = 1.36
c         else
c          if(d.le.4.5.and.d.ge.3.6) then
c          a = 0.0
c          b = 1.0
c          c = -0.4
c         else
c          if(d.le.5.5.and.d.ge.4.5) then
c           a = 0.4
c           b = -1.0
c           c = 2.2
c           endif
c          endif
c        endif
c       endif
c      y1 = (c-(a*d))/b
c       ene = ene + y1
c     endif
c       endif
c       enddo
c       enddo
cc      print *,ene
cc      stop

c********************************************

c       print *,'HB SET'
c       ik = 1
c       do i = 1,natp
c       ptyp = pat(i)
c       do j = 1,natom
c       ltyp = lat(j)
c      if((ptyp.eq.'D'.and.ltyp.eq.'A').or.
c     & (ptyp.eq.'D'.and.ltyp.eq.'DA').or.
c     & (ptyp.eq.'A'.and.ltyp.eq.'D').or.
c     & (ptyp.eq.'A'.and.ltyp.eq.'DA').or.
c     & (ptyp.eq.'DA'.and.ltyp.eq.'D').or.
c     & (ptyp.eq.'DA'.and.ltyp.eq.'A').or.
c     & (ptyp.eq.'DA'.and.ltyp.eq.'DA')) then
c       hbp(ik) = i
c       hbl(ik) = j
cc      print *,hbp(ik),hbl(ik),patnam(i)
c       ik = ik + 1
c       endif
c       enddo
c       enddo
cc      stop
c       ihcnt = ik

cc      print *,'STERIC SET'
c       ik = 1
cc      print *,natp,natom
c       do i = 1,natp
c       ptyp = pat(i)
c       do j = 1,natom
c       ltyp = lat(j)
c     if((ptyp.eq.'D'.and.ltyp.eq.'D').or.
c     & (ptyp.eq.'D'.and.ltyp.eq.'NP').or.
c    & (ptyp.eq.'A'.and.ltyp.eq.'A').or.
c     & (ptyp.eq.'A'.and.ltyp.eq.'NP').or.
c    & (ptyp.eq.'DA'.and.ltyp.eq.'NP').or.
c     & (ptyp.eq.'NP'.and.ltyp.eq.'A').or.
c    & (ptyp.eq.'NP'.and.ltyp.eq.'D').or.
c     & (ptyp.eq.'NP'.and.ltyp.eq.'DA').or.
c     & (ptyp.eq.'NP'.and.ltyp.eq.'NP')) then
c       sp(ik) = i
c       sl(ik) = j
cc      print *,sp(ik),sl(ik),patnam(i),presnam(i)
cc      print *,ptyp,ltyp
c       ik = ik + 1
c       endif
c       enddo 
c       enddo
c       iscnt = ik
cc      print *,'from global',ihcnt,iscnt
c       stop

c*******************************************************

c       print *,'CHECK HB SET'
c       print *,'no. of HB: ',ihcnt  
        do i = 1,ihcnt
        ip = hbp(i)
        il = hbl(i)
c       print *,ip,il 
        d = dist(px(ip,1),px(ip,2),px(ip,3),y(il,1),y(il,2),y(il,3))
c	write(31,*)i,'  ',d
c       print *,ptyp,'  ',ltyp,'  ',d 
        if(d.le.3.4) then
         if(d.le.2.3.and.d.ge.0) then
         a = 20.0 
         b = 2.3
         c = 46.0
         else
          if(d.le.2.6.and.d.ge.2.3) then
          a = 2.0
          b = 0.3
          c = 4.6
          else
           if(d.le.3.1.and.d.ge.2.6) then
           a = 0.0
           b = 1.0
           c = -2.0
         else
         if(d.le.3.4.and.d.ge.3.1) then
            a = 2.0
            b = -0.3
            c = 6.8
            endif
           endif
        endif
       endif
       y1 = (c-(a*d))/b
c       ene = ene + y1
        hb = hb + y1
       endif
        enddo
c	stop
c       print *,'CHECK STERIC SET'
c       print *,iscnt
        do j = 1,iscnt
        ip = sp(j)
        il = sl(j)
cc      print *,ip,il
        d = dist(px(ip,1),px(ip,2),px(ip,3),y(il,1),y(il,2),y(il,3))
c	write(31,*)i,'  ',pat(i),'  ',lat(i),'  ',d
c       print *,ptyp,'  ',ltyp,'  ',d
        if(d.le.5.5) then
         if(d.le.3.4.and.d.ge.0) then
         a = 20.0
         b = 3.4
         c = 68.0
         else
          if(d.le.3.6.and.d.ge.3.4) then
          a = 0.4
          b = 0.2
          c = 1.36
          else
           if(d.le.4.5.and.d.ge.3.6) then
           a = 0.0
           b = 1.0
           c = -0.4
         else
          if(d.le.5.5.and.d.ge.4.5) then
            a = 0.4
            b = -1.0
            c = 2.2
            endif
           endif
        endif
       endif
      y1 = (c-(a*d))/b
c       ene = ene + y1
        steric = steric + y1
      endif
      enddo

c       plpe = ene
c       print*,'hb,steric,both'
c       print *,hb,steric,hb+steric
        plpe = hb+steric
c	print*,'PLP',plpe
        if(tst.eq.2) then
c       write (*,*),'plp ene from minimiz: ',plpe,hb,steric
        endif
        return
        end
**************End of PLP********************************
	
c***********************************************************************

	function  rmmffene(ie,je)

        include 'mols.par'
        common/mout/opmo(maxstr,maxpar),opmi(maxstr,maxpar),
     &  emo(maxstr),emi(maxstr)
        common/crda/x(maxatm,8)
        common/crdb/y(maxatm,8)
        common/vectors/iv(maxpar,4)
        common /par/ natom, ntor, nhb, ns, lres
        common /tor/ u0(maxpar),sn(maxpar),tn(maxpar),phi(maxpar)
        common/pdbat/atom(maxatm),ele(maxatm)
        common /fnam/ if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4
        common/energy/e_final(maxord,maxord)
 
        common /getrot/inrot,irotb(maxi,2),el,ilsrot(maxi,maxi),
     &  iatrot(maxi),rx(100,3),ire(maxi,maxi),ind(maxi,maxi),big
        common /left/ le(maxi,maxi),innd(maxi),ielenum(maxi),
     &  bonum(maxi)
        
        common /gen/ ibno,iat1(maxi),iat2(maxi),isno(maxi)
        common /string/ atsym(maxi),desc(maxi),attyp(maxi),
     &  str1(maxi),str2(maxi),str3,res

        character*50 desc*30,attyp*10,atsym*6,str1*30,str2*30,
     &  str3*30,res*10,rmscr*35
	character*128 if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4,of0
        dimension pp(maxpar)

	character str*80,t*20
	real etot,rdihed,rvdw,reel
10      format(a80)
11      format(a25)
20      format(i3,i3,a8)
22      format(a6)
23      format(a3)
30      format(3x,i4,1x,a6,2x,3f10.4,1x,a29)
40      format(3f8.3)
60      format(1x,i5,1x,i5,1x,i5,a10)
70	format(a10,7x,f10.5)
	open(unit=50,file='cal_energy.mol2',status='unknown')

	do i=1,2
        write(50,11)str1(i)
        enddo
        write(50,20)natom,ibno,res
        do i=1,5
        write(50,11)str2(i)
        enddo
	do i=1,natom
c       write (50,15) atom(i),y(i,1),y(i,2),y(i,3),ele(i)
        write(50,30),innd(i),atsym(i),y(i,1),y(i,2),y(i,3),desc(i)
        enddo
        write(50,11)str3
        do j=1,ibno
        write(50,60)isno(j),iat1(j),iat2(j),attyp(j)
        enddo
c        return
        write(50,23)'TER'
        write(50,22)'ENDMDL'
	close(unit=50)
c	stop
	str= 'sh ene_script'
	call system(str)
	
        open(unit=113,file="output1",status= "old")

        read(113,*)rdihed
        read(113,*)rvdw
        read(113,*)reel

c        print*,rdihed,rvdw,reel
        etot= rdihed+rvdw+reel
c        print*,etot
        close(unit=113)

        rmmffene = etot
	rmscr='rm output1'
	call system(rmscr)
ccv	open(unit=51,file='ene1.txt',status='old')
ccv	read(51,70)t,etot 
c	er=ene
ccv	close(unit=51)
ccv	print*,'etotal',etot
	rmmffene = etot
	return
	end
c**********************************************************************
cc	subroutine flex
cc	include 'mols.par'
cc	parameter (maxm=20000)
cc      common /dcom/ipatom,pfile
cc        character pfile*128
cc	character xx4*4,xx3*3
cc	parameter(mxtyat = 18)
cc        common/ranges/jstart(maxatm,10),jend(maxatm,10),j1_4(maxatm,12)
cc        common/energy/e_final(maxord,maxord)
cc        common/mean/avrg1(maxpar,maxord)
cc        common /par/ natom, ntor, nhb, ns,lres,nlines,xx5,xx6,xx7,xx8
	
    
cc	character patnam(1000)*4,presnam(1000)*3,check*80,rpdb*80
cc	integer presno(500),psresno(500),natp,resno(500)
        
cc	natp = ipatom-1
cc10      format(12x,a4,1x,a3,1x,a1,8x,3f8.3)
cc101 	format(12x,a4,1x,a3,2x,I4)
cc102	format(a3,I3)
cc103 	format(a4,I4) 
cc        open(unit=1,file=pfile,status='unknown')
cc	open(unit=2,file='residues.txt',status='unknown')
c****** read details from receptor protein ******************************
cc	l = 0 
cc	do i = 1,natp
	
cc	read(1,101)patnam(i),presnam(i),presno(i)
c       read(1,10) patnam(i),presnam(i),pchid(i),(px(i,j),j=1,3)  
c	write(31,10)patnam(i),presnam(i),pchid(i),(px(i,j),j=1,3)
c-----------------------------------------------------------------------
cc	xx5 = presno(i)
cc	xx4 = patnam(i)
cc        xx3 = presnam(i)
	
cc	if(xx3(1:3).eq.'TYR')then
cc	  l = l + 1
cc	  psresno(i)=presno(i)
c	  write(2,*)l,psresno(i)
          
cc	  if(l.eq.1)then
cc	    write(2,102)presnam(i),psresno(i)
cc	  else
cc	    c = 1
cc	    do j = 2,l-1
cc	        xx6 = psresno(j)
cc		xx7 = psresno(i)
cc		xx8 = psresno(1)
c		write(2,*)xx5,xx6
cc		if(xx7.eq.xx6.or.xx7.eq.xx8)then
cc		c = c + 1
cc                else
cc                c = 0
cc	      endif
cc            enddo
c            write(2,*)c  
cc            if(c.eq.0)then
cc	     write(2,102)presnam(i),psresno(i)
cc	    endif
	   
cc          endif
cc	endif
cc       	enddo
cc	close(unit=1)
cc	close(unit=2)
c----------------------------------------------------------------------
cc	check = 'cat residues.txt | wc -l > lines.txt' 
cc	call system(check)
cc	open(unit=3,file='lines.txt',status='unknown')
cc	read(3,*)nlines !nlines = no. of residues matching the resname 
cc	close(unit=3)
c----------------------------------------------------------------------

cc	rpdb='sh script2'
cc	call system(rpdb)

c ---read the co-ordinates of the selected amino acid from the receptor
cc	do i = 1,nlines
cc	enddo
cc	return
cc	end

c*******************receptor side chain flexibility********************
	subroutine flexall
	include 'mols.par'
c       common /par/ ptor
        common /getrot/ipprot,ipprotb(maxi,2),el

        integer nlines,aline,iprot,iprotb(100,2),frotb(100,2),ptor,nb
	character check*80,cpy*80,rbnd*80,str*80,ibnd*80
c	ptor=0
	check = 'cat residues.txt|wc -l>lines.txt'
        call system(check) 
        open(unit=3,file='lines.txt',status='unknown')
        read(3,*)nlines !nlines = no. of residues in residues.txt 
c	print *,nlines
        close(unit=3)
	do i=1,nlines
          cpy='sh scriptres'
          call system(cpy)
   
          ibnd='cat flexres|wc -l>aline.txt'
          call system(ibnd)

          rbnd='sh script2'
          call system(rbnd)
c          call pstore_index
          
10        format(a9,1x,i3)
cc20      format(a16,1x,i3)
30        format(i2,1x,i2)
cc40      format(i2,6x,i2)
          open(unit=1,file='flexres.aux',status='old')
          open(unit=2,file='rbnd',status='unknown')
          
          read(1,10)str,ipprot !str=NROTBONDS,inrot=number of rotatable bonds
cc        ptor=ptor+iprot !ntor = number of torsions = number of rotatable bonds
c         write(*,10)str,iprot
c         print *,str,iprot
          do l=1,ipprot
            read(1,30)(ipprotb(l,m),m=1,2)!ipprotb(i,1)=bonding atom no,irotb(i,2)=bond atom no
cc          read(1,20)str,el !str = Num_array_elment,el=0 
          enddo
cc          nb=0      
          do r=1,ipprot
           if(ipprotb(r,2).ne.2)then
            if(ipprotb(r,2).ne.3)then
             write(2,30)(ipprotb(r,s),s=1,2)
            endif 
           endif
          enddo
         close(unit=2)
         close(unit=1)      
         
        call flexrot 
          
	enddo
	return
	end

c***********************************************************************        
        subroutine flexrot
c       program flexrot
        include 'mols.par'
        integer frotb(maxi,2)
        integer aline,fline
        character ibnd*80,ifnd*80
        dimension xx_one(maxatm,3)
100     format(i2,1x,i2)

        ifnd='cat rbnd|wc -l>fline.txt'
        call system(ifnd)
        open(unit=1,file='fline.txt',status='unknown')
        read(1,*)fline
        close(unit=1)

        open(unit=2,file='rbnd',status='old')
        do i=1,fline
        read(2,100)(frotb(i,j),j=1,2)
        print *,frotb(i,1),frotb(i,2)
        enddo
        close(unit=2)
        
        open(unit=3,file='aline.txt',status='unknown')
        read(3,*)aline!aline=no. of atoms in a residue
        close(unit=3)
        
101     format(30x,3f8.3)
        open(unit=4,file='flexres',status='old')
        do j=1,aline
        read(4,101)(xx_one(j,k),k=1,3)
c       write(*,*),(xx_one(j,k),k=1,3)  
        enddo
        close(unit=4)

cc        do ig=1,fline

cc        call elemen(xx_one(frotb(ig,1),1),xx_one(frotb(ig,1),2),
cc     &              xx_one(frotb(ig,1),3),
cc     &              xx_one(frotb(ig,2),1),xx_one(frotb(ig,2),2),
cc     &              xx_one(frotb(ig,2),3),
cc     &              el,em,en)

c        print *,el,em,en
                

cc        enddo

        return
        end
c**********************************************************************
        subroutine pstore_index
        include 'mols.par'
c       program to store the indices of the rotatable bonds and its
c       neighbours
c       parameter (maxi=1000)

        common /par/ patom, ptor
        common /getrot/iprot,iprotb(maxi,2),iplsrot(maxi,maxi),
     &   ipatrot(maxi),pind(maxi,maxi)

        
     
        character pstr*80
        integer pel,ptem(35)
c       integer numrot,bonum(50)   !declaration for remaining atoms list

10      format(a9,1x,i3)
20      format(a16,1x,i3)
30      format(i3,5x,i3)
40      format(i4)
50      format(a19,1x,i4)
60      format(a8,1x,i4)
70      format(a13,1x,i4)
        
        
        open(unit=3,file='aline.txt',status='unknown')
        read(3,*)patom!patom=no. of atoms in a residue
        close(unit=3)

        
        open(unit=4,file='flexres.out',status='old')
        open(unit=2,file='rbnd.out',status='unknown')
        read(4,10)pstr,iprot !str=NROTBONDS,iprot=number of rotatable
                            !bonds
        ptor=iprot !ntor = number of torsions = number of rotatable
                    !       bonds
        print *,'ptor',ptor
c       write(*,10)str,inrot
c       print *,str,inrot
        do i=1,iprot
          read(4,30)(iprotb(i,j),j=1,2)!irotb(i,1)=bonding atom
                                      !no,irotb(i,2)=bond atom no
          read(4,20)pstr,pel !str = Num_array_elment,el=0 
          ipatrot(i) = pel
          ptem(i)=patom-ipatrot(i)
          print *,'pdbgen,atrot',ipatrot(i)
          print *,'pdbgen,tem',ptem(i)
              do k=1,ipatrot(i)
                 read(4,40)iplsrot(i,k)
c                ire(i,k)=ilsrot(i,k)
                 pind(i,iplsrot(i,k))=1
              enddo
        enddo
c       print *,'cont'
        write(2,50)'number of rot_bonds',iprot
        do i=1,iprot
        write(2,60)'Rot_Bond',i
        write(2,70)'No.of Element',ptem(i)
        write(*,*)'Rot_Bond',i
        write(*,*)'No. of Element',ptem(i)
         do k=1,patom
            if (pind(i,k).ne.1) then
              write(2,40),k
            endif
          enddo
        enddo
        close(unit=2)
        
        return
        end

















