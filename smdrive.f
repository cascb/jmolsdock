c     library name : smdrive.f   

c     Copyright (c) 2013 P.Arun Prasad, S.Nehru Viji and N.Gautham

c     This library is free software; you can redistribute it and/or
c     modify it under the terms of the GNU Lesser General Public
c     License as published by the Free Software Foundation; either
c     version 2.1 of the License, or (at your option) any later version.
c
c     This library is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
c     Lesser General Public License for more details.
c
c     You should have received a copy of the GNU Lesser General Public
c     License along with this library; if not, write to the Free
c     Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
c     02110-1301  USA
c
c
c     contact : n_gautham@hotmail.com  

	program MAIN
        include 'mols.par'
        parameter (maxn=2000)
        common /par/ natom, ntor, nhb, ns, lres
        common /tor/ u0(maxpar),sn(maxpar),tn(maxpar),phi(maxpar)
	common /ctl/iopt,iff,icint,fseq(maxres)
	common /cen/bx,by,bz
        common /fnam/ if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4
        common/mout/opmo(maxstr,maxpar),opmi(maxstr,maxpar),
     &              emo(maxstr),emi(maxstr)
	common /dcom/ipatom,pfile,path,mname

        character seq*(maxres),mname*128,path*128
        character pfile*128,mol*128,convert*80,lfile*128,str*80
        character*128 if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4,of0
c
	CHARACTER*8 HOUR
	CHARACTER*9 DAY
	real tt(2),bx,by,bz
        integer latom

        open(unit=30,file='user.inp',status='old')
        read(30,'(A)') path
c       print *,'path name :  ',path
        read(30,*) mname
c       print *,'molecule name :  ',mname
c
c***********Out put generate file names**********************
	of0 = path(:LNBLNK(path)) // '/' //
     &        mname(:LNBLNK(mname)) // '_' // 'inf.log'
	pf2 = path(:LNBLNK(path)) // '/' //
     &        mname(:LNBLNK(mname)) // '_' // 'mols.mol2'
	pf3 = path(:LNBLNK(path)) // '/' //
     &        mname(:LNBLNK(mname)) // '_' // 'mini.mol2'
	of1 = path(:LNBLNK(path)) // '/' //
     &        mname(:LNBLNK(mname)) // '_' // 'mols.out'
	of2 = path(:LNBLNK(path)) // '/' //
     &        mname(:LNBLNK(mname)) // '_' // 'mini.out'
c       print *,if1,of0,of1,pf1,pf2,pf3,pf4,of1,of2,of3,of4
c************************************************************
        open(unit=31,file=of0,status='unknown')
      WRITE (31,1)
      WRITE (*,1)
1     FORMAT (//
     *10X,'#########################################################'/
     *10X,'#                                                       #'/
     *10X,'#                      MOLSDOCK                         #'/
     *10X,'#                                                       #'/
     *10X,'#          CAS in Crystallography and Biophysics,       #'/
     *10X,'#    University of Madras, Chennai - 600 025, INDIA     #'/
     *10X,'#                                                       #'/
     *10X,'#########################################################'/)
  
        CALL DATE(DAY)
        CALL TIME(HOUR)
        
        
	WRITE (*,'(2(/25X,2A/))') 'Job started on ', DAY,
     &                               '      at ', HOUR
	WRITE (31,'(2(/25X,2A/))') 'Job started on ', DAY,
     &                               '      at ', HOUR
c--------read inputs-------------------------------
        print *,'molecule name :  ',mname
        read(30,'(A)')lfile
!

!
c	open(unit=12,file='inp.mol',status='unknown')
c       write(12,'(A)')mol
c       close(unit=12)

10      format(a80)
11      format(a6)        
        open(unit=1,file=lfile,status='unknown')
        do i=1,maxn
        read(1,10,end=99),str
        enddo
99      write(*,*),'line number',i
        close(1)
        latom=i-1

        print *,latom

        open(unit=2,file=lfile,status='unknown')
        open(unit=3,file='ligand.mol',status='unknown')
        do k=1,latom
        read(2,10)str
        if(k.eq.1)then
        write(3,11)'ligand'
        else
        write(3,10)str
        endif
        enddo
        close(3)
        close(2)


        convert='sh fileform.sh'
        call system(convert)


	print*,lfile
	read(30,*) ns
c	write(*,*) ns
c       ns=1500
	read(30,*) is,ie
c	print*,'is',is,'ie',ie
	read(30,*)bx,by,bz
c	print*,'bx=',bx,'by=',by,'bz=',bz
        iopt=2
c	read(30,*) iopt
        read(30,'(A)') pfile
	print*,pfile
	call iread_pdb
c	stop
c        read(30,*) irun
c        read(30,*) iff
c        read(30,*) ico
c        read(30,*) icnt
c        read(30,*) rk1,kr2
c

c******************************************************
2       format(/a55/)
        write(31,2)'***********GENERATING INITIAL COORDINATES*********'
        write(*,2)'***********GENERATING INITIAL COORDINATES**********'
	write(31,*)'The MOL file is  :',lfile
	write(*,*)'The MOL file is:',lfile
c 	call flex
	call pdbgen
	print*,'pdbgen o.k.'
        call varinit

c***********************************************************************
        write(31,2)'*******GENERATING MOLS OPTIMAL & MINIMIZED STRUCTUR
     &ES**********'
        write(*,2)'******GENERATING MOLS OPTIMAL & MINIMIZED STRUCTURES*
     &***********'
c	call conformation(0)
	do inp=is,ie
 	call mols(inp,iopt)
	print*,'molgen o.k.'
	call conformation(1,inp)
	print*,'conformation o.k'
c	stop
c	CALL DATE(DAY)
c	CALL TIME(HOUR)
c        WRITE (31,'(2(/25X,2A/))') 'minimization started on ', DAY,
c     &                               '      at ', HOUR
c     	WRITE (*,'(2(/25X,2A/))') 'minimization  started on ', DAY,
c     &                               '      at ', HOUR
c	print*,'minimization started at'
c	irun=3
	call minimiz(inp)
cv	call minimiz
cv	call conformation(2,inp)
	enddo
        CALL DATE(DAY)
        CALL TIME(HOUR)
        WRITE (31,'(2(/25X,2A/))') 'Job ended on ', DAY,
     &                               '      at ', HOUR
        WRITE (*,'(2(/25X,2A/))') 'Job ended on ', DAY,
     &                               '      at ', HOUR
        write (*,*)'cpu time = : ',etime(tt)
        write(31,*)'cpu time = : ',etime(tt)


	stop
	end
c*************************************************************************
!!Subroutine to find the number of atoms in the receptor (protein) molecule.
	subroutine iread_pdb
	parameter (maxm=20000)
        common /dcom/ipatom,pfile,path,mname
        character pfile*128,str*80,atm*4,str1*80
        integer opatom,opatm,ll
10	format(a80)
20      format(a4,a76)
        open(unit=1,file=pfile,status='unknown')
	do i=1,maxm
        read(1,10,end=99),str
        enddo
99      write(*,*),'line number',i
        close(unit=1)

        opatom=i-1 !no. of lines in original receptor PDB

        write(*,*),'opatom',opatom

        open(unit=1,file=pfile,status='unknown')
        open(unit=2,file='pfile_orig.pdb',status='unknown')
        do k=1,opatom
        read(1,10)str
        write(2,10)str
        enddo
        close(2)
        close(1)
        
        opatm=k-1        
        ll=1
        write(*,*),'opatm',opatm
        open(unit=1,file=pfile,status='unknown')
        open(unit=2,file='pfile_mod.pdb',status='unknown')
        do l=1,opatm
        read(1,20)atm,str1
        if(atm.eq.'ATOM')then
        ll=ll+1
        write(2,20)atm,str1
        endif
        enddo
        close(2)
        close(1)

        ipatom=ll-1 !#1-- no. of lines in modified receptor PDB
                    !#2--modified receptor PDB[pfile_mod.pdb] has only 
                    !'ATOM' of the original receptor PDB[pfile_orig.pdb] 

        write(*,*)'ipatom',ipatom

        open(unit=1,file='pfile_mod.pdb',status='unknown')
        open(unit=2,file=pfile,status='unknown')
        do m=1,ipatom
        read(1,10)str
        write(2,10)str
        enddo
        close(2)
        close(1)

c       write(*,*)ipatom
c100    stop
c       close(unit=1)
	return
        end
c***find rotable bonds in the selected residues of the receptor protein**
	subroutine flex()
        include 'mols.par'
        parameter (maxm=20000)
        common /dcom/ipatom,pfile
        character pfile*128
        character xx4*4,xx3*3
        parameter(mxtyat = 18)
        common/ranges/jstart(maxatm,10),jend(maxatm,10),j1_4(maxatm,12)
        common/energy/e_final(maxord,maxord)
        common/mean/avrg1(maxpar,maxord)
        common /par/ natom, ntor, nhb, ns,lres,nlines,xx5,xx6,xx7,xx8

   
        character patnam(1000)*4,presnam(1000)*3,check*80,rpdb*80
        integer presno(500),psresno(500),natp,resno(500)

        natp = ipatom-1
10      format(12x,a4,1x,a3,1x,a1,8x,3f8.3)
101     format(12x,a4,1x,a3,2x,I4)
102     format(a3,I3)
103     format(a4,I4)
        open(unit=1,file=pfile,status='unknown')
        open(unit=2,file='residues.txt',status='unknown')
c****** read details from receptor protein ******************************
        l = 0
        do i = 1,natp

        read(1,101)patnam(i),presnam(i),presno(i)
c       read(1,10) patnam(i),presnam(i),pchid(i),(px(i,j),j=1,3)
c       write(31,10)patnam(i),presnam(i),pchid(i),(px(i,j),j=1,3)
c-----------------------------------------------------------------------
        xx5 = presno(i)
        xx4 = patnam(i)
        xx3 = presnam(i)

        if(xx3(1:3).eq.'TYR')then
          l = l + 1
          psresno(i)=presno(i)
c         write(2,*)l,psresno(i)

          if(l.eq.1)then
            write(2,102)presnam(i),psresno(i)
          else
            c = 1
            do j = 2,l-1
                xx6 = psresno(j)
                xx7 = psresno(i)
                xx8 = psresno(1)
c               write(2,*)xx5,xx6
                if(xx7.eq.xx6.or.xx7.eq.xx8)then
                c = c + 1
                else
                c = 0
              endif
            enddo
c            write(2,*)c
            if(c.eq.0)then
             write(2,102)presnam(i),psresno(i)
            endif

          endif
        endif
        enddo
        close(unit=1)
        close(unit=2)
c----------------------------------------------------------------------
cc        check='cat residues.txt | wc -l > lines.txt'
cc        call system(check)
cc        open(unit=3,file='lines.txt',status='unknown')
cc        read(3,*)nlines !nlines = no. of residues matching the resname
cc        close(unit=3)
	return
	end
c**********************************************************************
        subroutine rec_prep
        include 'mols.par'
        common /dcom/opatom,ipatom,pfile,path,mname
        character pfile*128,str*80,atm*4,str1*80

10      format(a80)
20      format(a4,a76)
        open(unit=1,file=pfile,status='unknown')
        open(unit=2,file='pfile_orig.pdb',status='unknown')
        do i=1,ipatom
        read(1,10)str
        write(2,10)str
        enddo        
        close(2)
        close(1)

        open(unit=1,file=pfile,status='unknown')
        open(unit=2,file='pfile_mod.pdb',status='unknown')
        do i=1,opatom
        read(1,20)atm,str1
        if(atm.eq.'ATOM')then
        write(2,20)atm,str1
        endif
        enddo
        close(2)
        close(1)

        open(unit=1,file='pfile_mod.pdb',status='unknown')
        open(unit=2,file=pfile,status='unknown')
        do i=1,ipatom
        read(1,10)str
        write(2,10)str
        enddo
        close(2)
        close(1)
        return
        end
c**********************************************************************






