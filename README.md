###JMOLSDOCK
####INTRODUCTION :
JMOLSDOCK is a Java-based GUI for MOLSDOCK, a 'flexible ligand-rigid receptor' docking algorithm.  MOLSDOCK uses mutually orthogonal Latin squares(MOLS) to simultaneously sample both the docking space and the ligand conformation. 

####If you use JMOLSDOCK, please cite:
**Sam Paul, D. and Gautham, N.(2013). JMOLSDOCK : A JAVA-based GUI for MOLSDOCK, Journal of the Madras University,Section B.66:7-10.ISSN 0368-3184.**
(paper attached in the 'Downloads' section)

####LICENSE: 
JMOLSDOCK is a free, open-source software licensed under the GNU Lesser General Public License. 

####DOWNLOAD : 
JMOLSDOCK source code and binary are available for Linux distributions at https://bitbucket.org/cascb/jmolsdock/downloads 
####INSTALLATION :
After downloading Jmolsdock-binary.tar.gz, extract  the file and execute the following commands in Jmolsdock directory.
````
$ tar -xzf Jmolsdock-binary.tar.gz
$ cd Jmolsdock
$ sh install.sh
````
Note: For installation user must have Administrative privilages.

To launch JMOLSDOCK  
click -->**Applications**-->**Education**-->**JMOLSDOCK**  

For more information on installation and usage of JMOLSDOCK, please refer JMOLSDOCK wiki at https://bitbucket.org/cascb/jmolsdock/wiki/Home

####Acknowledgements:
The development of JMOLSDOCK is the result of a team effort and in particular contributions from **Vengadesan**, **Arun Prasad** and **Nehru Viji** are acknowledged.