ifort -c smdrive.f smpdbgen.f smvarinit.f smmols.f smconformation.f smminimiz.f
ifort -o molsdock smdrive.o smpdbgen.o smvarinit.o smmols.o smconformation.o smminimiz.o

ifort -c drive.f pdbgen.f amppar.f ecppar.f varinit.f conformation.f mols.f minimiz.f cluster.f mclust.f
ifort -o lmols drive.o pdbgen.o amppar.o ecppar.o varinit.o conformation.o mols.o minimiz.o cluster.o mclust.o

ifort -c cpyres.f
ifort -o cpyres cpyres.o

ifort -c read_ene.f
ifort -o interene read_ene.o

ifort -c prelims.f 
ifort -o centroid prelims.o

cp centroid receptor/ 
