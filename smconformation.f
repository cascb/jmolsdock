c     library name : smconformation.f   

c     Copyright (c) 2013 P.Arun Prasad, S.Nehru Viji and N.Gautham

c     This library is free software; you can redistribute it and/or
c     modify it under the terms of the GNU Lesser General Public
c     License as published by the Free Software Foundation; either
c     version 2.1 of the License, or (at your option) any later version.
c
c     This library is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
c     Lesser General Public License for more details.
c
c     You should have received a copy of the GNU Lesser General Public
c     License along with this library; if not, write to the Free
c     Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
c     02110-1301  USA
c
c
c     contact : n_gautham@hotmail.com  

        subroutine conformation(kk,inp)
c	program to write pdb file from cg output

	include 'mols.par'
	common/mout/opmo(maxstr,maxpar),opmi(maxstr,maxpar),
     &  emo(maxstr),emi(maxstr)
        common/crda/x(maxatm,8) 
        common/crdb/y(maxatm,8) 
        common/vectors/iv(maxpar,4)
        common /par/ natom, ntor, nhb, ns, lres
	common /tor/ u0(maxpar),sn(maxpar),tn(maxpar),phi(maxpar)
	common/pdbat/atom(maxatm),ele(maxatm)
	common /fnam/ if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4
	
	common /getrot/inrot,irotb(maxi,2),el,ilsrot(maxi,maxi),
     &  iatrot(maxi),rx(100,3),ire(maxi,maxi),ind(maxi,maxi),big
        common /left/ le(maxi,maxi),innd(maxi),ielenum(maxi),
     &  bonum(maxi)
	
	common /gen/ ibno,iat1(maxi),iat2(maxi),isno(maxi)
        common /string/ atsym(maxi),desc(maxi),attyp(maxi),
     &  str1(maxi),str2(maxi),str3,res
	 common /cen/bx,by,bz
        common /plp/ pat(mnatp), lat(maxatm),patnam(mnatp),
     & presnam(mnatp),pchid(mnatp),
     &  tpres(25), tatnam(25,50), natp,
     & tatyp(25,50), ntatp(25), px(mnatp,3)
  

	character*50 desc*30,attyp*10,atsym*6,str1*30,str2*30,
     &  str3*30,res*10

	character*128 if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4,of0
	dimension pp(maxpar)
	character*10 pdbfile
	character*30 atom
        character*24 ele
15	format(a30,3f8.3,a24)
cc21	format(a5,4x,i4,3xf5.2)
21	format(a5,4x,i4,3x,f10.2)
22	format(a6)
23      format(a3)
24      format(10f10.3)
25      format(9f10.3)
26      format(10a8)
27      format(9a8)
10      format(a80)
11      format(a25)
12	  format(a17)
13	  format(a13)
20      format(i3,i3,a8)
30      format(3x,i4,1x,a6,2x,3f10.4,1x,a29)
40      format(3f8.3)
60      format(1x,i5,1x,i5,1x,i5,a10)





	nn = ntor
c	call pdb
c        do i=1,natom
c          do j=1,8
c             y(i,j)=x(i,j)
c          enddo          
c	enddo
        open(unit=80,file='molsene.out',status='unknown')
        open(unit=81,file='miniene.out',status='unknown')
        if(kk.eq.1) then
        if(iff.eq.1) write(80,26) 'Estatic','vdw','HB','ES1-4','NB1-4'
     &  ,'AMBER','PHB','PSteric','PLP-ENE','TOTAL'
        if(iff.eq.2) write(80,26) 'Estatic','vdw','HB','Tor'
     &  ,'ECEPP','PHB','PSteric','PLP-ENE','TOTAL'
        else
        if(iff.eq.1) write(81,27) 'Estatic','vdw','HB','ES1-4','NB1-4'
     &  ,'AMBER','PHB','PSteric','PLP-ENE','TOTAL'
        if(iff.eq.2) write(81,27) 'Estatic','vdw','HB','Tor',
     &  'ECEPP','PHB','PSteric','PLP-ENE','TOTAL'
        endif


	if(kk.eq.1) then
	open(unit=50,file=pf2,access='append')
	elseif(kk.eq.2)then
	open(unit=50,file=pf3,access='append')
	elseif(kk.eq.0)then
	open(unit=50,file='molsx.pdb',status='unknown')
	goto 313
	endif
	do k11=inp,inp!1,ns
cc10	format(1x,19(1x,f5.1),2x,f15.4)
cc11	format(a10)
	if(kk.eq.1) then
	do j = 1,nn+6
	pp(j) = opmo(k11,j)
	phi(j) = opmo(k11,j)
	enddo

        do j = nn+1,nn+3
        tt1 = opmo(k11,j)
        if(tt1.eq.0.0) then
        tt2 = 0.0
        else
c       tt2 = (tt1/10.0)*.28
        tt2 = (tt1/10.0)*0.13888889
c       tt2 = (tt1/10.0)*0.55555556
        endif
c       tt2 = tt2-5.0
        tt2 = tt2-2.5
c       tt2 = tt2-10.0
        pp(j) = tt2
        enddo
	ent = emo(k11)


	elseif(kk.eq.2) then

	do j = 1,nn+6
	pp(j) = opmi(k11,j)
	phi(j) = opmi(k11,j)
	enddo
        do j = nn+1,nn+3
        tt1 = opmo(k11,j)
        if(tt1.eq.0.0) then
        tt2 = 0.0
        else
c       tt2 = (tt1/10.0)*.28
        tt2 = (tt1/10.0)*0.13888889
c       tt2 = (tt1/10.0)*0.55555556
        endif
c       tt2 = tt2-5.0
        tt2 = tt2-2.5
c       tt2 = tt2-10.0
        pp(j) = tt2
        enddo
	ent = emi(k11)

	endif
c	print *,'from conformation'
c	write(*,*) k11,ent,(opmo(k11,k),k=1,nn)

      call molgen3(pp)
c      print *,k11,ampene(0,0)
	open(unit=551,file="mols.mol2",status="unknown")

c	write(50,21)'MODEL',k11,ent
c	write(551,21)'MODEL',k11,ent
	do i=1,2
        write(50,12)str1(i)
        write(551,12)str1(i)
        enddo
        write(50,20)natom,ibno,res
        write(551,20)natom,ibno,res
        do i=1,5
        write(50,13)str2(i)
        write(551,13)str2(i)
        enddo
c	write(50,*),''
313	do i=1,natom
c	write (50,15) atom(i),y(i,1),y(i,2),y(i,3),ele(i)
	write(50,30),innd(i),atsym(i),y(i,1),y(i,2),y(i,3),desc(i)
	write(551,30),innd(i),atsym(i),y(i,1),y(i,2),y(i,3),desc(i)
	enddo
	write(50,11)str3
	write(551,11)str3
        do j=1,ibno
        write(50,60)isno(j),iat1(j),iat2(j),attyp(j)
        write(551,60)isno(j),iat1(j),iat2(j),attyp(j)
        enddo
	write(50,23)'TER'
	write(551,23)'TER'
	write(50,22)'ENDMDL'
	write(551,22)'ENDMDL'
c        return
	close(unit=551)
        call eplp(plpe,hb,steric)
        open(unit=122,file='rplp.txt',status='unknown')
        write(122,*)plpe
	close(unit=122)


	energy = rmmffene(k11,k11)
c        if(iff.eq.1) then
c        energy = ampene(k11,k11)
c        else
c        energy = ecpene(k11,k11)
c        endif

        if(kk.eq.1) then
        if(iff.eq.1) then
        write (80,24)enb,ees,ehb,enb1_4,ees1_4,energy,
     &          hb,steric,plpe,plpe+energy
                write (*,*)enb,ees,ehb,enb1_4,ees1_4,energy,
     &          hb,steric,plpe,plpe+energy
        endif
        if(iff.eq.2) then
                write (80,25)ees,enb,ehb,etor,energy,
     &          hb,steric,plpe,plpe+energy
                write (*,*)ees,enb,ehb,etor,energy,
     &          hb,steric,plpe,plpe+energy
        endif
        else
        if(iff.eq.1) then
        write (81,24)enb,ees,ehb,enb1_4,ees1_4,energy,
     &          hb,steric,plpe,plpe+energy
                write (*,*)enb,ees,ehb,enb1_4,ees1_4,energy,
     &          hb,steric,plpe,plpe+energy
        endif
        if(iff.eq.2) then
                write (81,25)ees,enb,ehb,etor,energy,
     &          hb,steric,plpe,plpe+energy
                write (*,*)ees,enb,ehb,etor,energy,
     &          hb,steric,plpe,plpe+energy
        endif
        endif

c	write(50,23)'TER'
c	write(50,22)'ENDMDL'
	if(kk.eq.0)return

	enddo
	close(unit=50)
	close(unit=80)
	close(unit=81)

	return
	end
c************************************************************************
        subroutine molgen3(phi)
	include 'mols.par'
        common/parameters/e(maxord,maxord,maxpar),p(maxpar,maxord,3)
        common/crda/x(maxatm,8)
        common/crdb/y(maxatm,8)
        common /par/ natom, ntor, nhb, ns, lres
        common/vectors/iv(maxpar,4)
        common/order/nn,mm
        common /getrot/inrot,irotb(maxi,2),el,ilsrot(maxi,maxi),
     &   iatrot(maxi),rx(100,3),ire(maxi,maxi),ind(maxi,maxi),big
        common /left/ le(maxi,maxi),innd(maxi),ielenum(maxi),
     &  bonum(maxi)
	 common /cen/bx,by,bz

c       integer le(maxi,maxi),elenum(maxi,maxi)

        dimension x_one(maxatm,3),x_two(maxatm,3)
        dimension phi(maxpar)
        real bx,by,bz
c 
c       write(31,*)(phi(ii),ii=1,nn)
c        write(*,*)(phi(ii),ii=1,nn)

c        bx= -9.665
c        by= 16.179
c        bz= 1.860

        nn=ntor

        do k=1,natom
           do ki=1,3
             x_one(k,ki)=rx(k,ki)
c       write(*,*)ix_one(k,ki)
           enddo
        enddo
        do if=1,nn

C###################### PHI ALL ####################################

        call elemen(x_one(irotb(if,1),1),x_one(irotb(if,1),2),
     &              x_one(irotb(if,1),3),
     &              x_one(irotb(if,2),1),x_one(irotb(if,2),2),
     &              x_one(irotb(if,2),3),
     &              el,em,en)


50      format(i4,1x,3f8.3)
        do j=1,ielenum(if)
        k=le(if,j)
           do ki=1,3
           x_two(k,ki)=x_one(k,ki)
c       write(*,50)k,x_two(k,ki)
           enddo
        enddo

c       write(*,50)k,x_two(k,ki)
c       stop
c------------------------------------------------------
                                                                                                                                   
c        do k=1,iv(if,3)-1
c           do ki=1,3
c             x_two(k,ki)=x_one(k,ki)
c           enddo
c        enddo
c------------------------------------------------------
         do j=1,iatrot(if)
          k=ilsrot(if,j)

c         write(*,*)
           xin=x_one(k,1)-x_one(irotb(if,1),1)
           yin=x_one(k,2)-x_one(irotb(if,1),2)
           zin=x_one(k,3)-x_one(irotb(if,1),3)
           call rotor(el,em,en,phi(if),xin,yin,zin,
     &                xout,yout,zout)
c        print*,'phi',el,em,en,phi(if)
c           write(*,*)'xoout',xout,yout,zout
           x_two(k,1)=xout+x_one(irotb(if,1),1)
           x_two(k,2)=yout+x_one(irotb(if,1),2)
           x_two(k,3)=zout+x_one(irotb(if,1),3)
        

c       print*,'rot_coord'
c        write(21,50),k,x_two(k,1),x_two(k,2),x_two(k,3)
        enddo

        do k=1,natom
           do ki=1,3
              x_one(k,ki)=x_two(k,ki)
           enddo
        enddo
C####################################################################

        enddo

        do k=1,natom
           do ki=1,3
             y(k,ki)=x_two(k,ki)
c        write(61,50)k,y(k,ki)
           enddo
        enddo

c	do k=1,natom
c	 do ki=1,3
c        write(61,50)k,(y(k,ki),ki=1,3)
c	 enddo
c	enddo

c       print *,'CONFORMATION CHECK'
c       print *,'nn = ',nn
c       stop
c       write(*,*) (phi(k),k=1,nn+6)
c       print *,'final trans',phi(nn+1),phi(nn+2),phi(nn+3)
   
        call rotate(bx,by,bz,phi(nn+4),phi(nn+5),phi(nn+6),natom)
        call translate(bx,by,bz,phi(nn+1),phi(nn+2),phi(nn+3),natom)
  
c       print *,'final trans',phi(nn+1),phi(nn+2),phi(nn+3)


c        stop
        return
        end
c***********************************************************************
     
     
       
                                                               

