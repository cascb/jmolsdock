c     library name : smpdbgen.f   

c     Copyright (c) 2013 P.Arun Prasad, S.Nehru Viji and N.Gautham

c     This library is free software; you can redistribute it and/or
c     modify it under the terms of the GNU Lesser General Public
c     License as published by the Free Software Foundation; either
c     version 2.1 of the License, or (at your option) any later version.
c
c     This library is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
c     Lesser General Public License for more details.
c
c     You should have received a copy of the GNU Lesser General Public
c     License along with this library; if not, write to the Free
c     Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
c     02110-1301  USA
c
c
c     contact : n_gautham@hotmail.com  






	subroutine pdbgen
c	program pdbgen
c	Program to call the smi23d program
        include 'mols.par'

c	parameter (maxi=1000)

        common /crdb/y(maxatm,8)
	common /par/ natom, ntor, nhb, ns, lres
        common /getrot/inrot,irotb(maxi,2),el,ilsrot(maxi,maxi),
     &   iatrot(maxi),rx(100,3),ire(maxi,maxi),ind(maxi,maxi),big
	common /left/ le(maxi,maxi),innd(maxi),ielenum(maxi),
     &	bonum(maxi)
        common /gen/ ibno,iat1(maxi),iat2(maxi),isno(maxi)
        common /string/ atsym(maxi),desc(maxi),attyp(maxi),
     &  str1(maxi),str2(maxi),str3,res
    
	character*50 desc*30,attyp*10,atsym*6,str1*30,str2*30,
     &  str3*30,res*10
	integer el
	call prep_coord
        write(31,*)'No.of rotatable bonds:',ntor
        write(31,*)'No. of atoms         :',natom

	end

c	*******************************************************************
	subroutine prep_coord
	include 'mols.par'
	character pdb*80,str*80,str1*80,str2*80,rpdb*80,pdb1*80

	pdb ='sh script1'  ! this script calls the routines for 3d 
	                   ! coordinates and calcluates initial conformation 
                           ! energy
        
        call system(pdb)

        call lchange
        pdb1='sh script01'
c	rpdb='sh script2'
	call system(pdb1)
c	call system(rpdb)
		
	call coorgen
	call store_index

c	call rcoorgen! this is not needed for the receptor
c	call rstore_index
	end
c	--------------------------------------------------------------------
        subroutine lchange
        include 'mols.par'
        
        character str*80,str3*80
        character desc*30,attyp*10,atsym*6,str1(maxi)*30,str2(maxi)*30
        character  res*10,str4(maxi)*80,str5(maxi)*80,str41(maxi)*80,
     &  str42(maxi)*80
        integer atno,ibno,natom
        real r,big

10      format(a80)
11      format(a25)
20      format(i3,i3,a8)
40      format(3f8.3)
50      format(a7,1x,a4,a64)
60      format(a7,1x,a1,3x,a64)


        open(unit=1,file='lout.mol2',status='old')
        open(unit=2,file='out.mol2',status='unknown')       
        do i=1,2
        read(1,11)str1(i)
        write(2,11)str1(i)
        enddo

        read(1,20)atno,ibno,res
        write(2,20)atno,ibno,res

        natom=atno
        do i=1,5
        read(1,11)str2(i)
        write(2,11)str2(i)
        enddo

        do i=1,atno

        read(1,50),str4(i),str41(i),str42(i)
        write(2,60),str4(i),str41(i),str42(i)

        enddo

        read(1,11)str3
        write(2,11)str3
 
        do j=1,ibno
        read(1,10)str5(j)
        write(2,10)str5(j)
        enddo

        return
        end
c----------------------------------------------------------------------------
	subroutine store_index
        include 'mols.par'
c       program to store the indices of the rotatable bonds and its neighbours
c	parameter (maxi=1000)

       	common/crdb/y(maxatm,8)
	common /par/ natom, ntor, nhb, ns, lres
        common /getrot/inrot,irotb(maxi,2),el,ilsrot(maxi,maxi),
     &   iatrot(maxi),rx(100,3),ire(maxi,maxi),ind(maxi,maxi),big
	common /left/ le(maxi,maxi),innd(maxi),ielenum(maxi),
     &	bonum(maxi)

        character str*80
        integer el,tem(35)
c       integer numrot,bonum(50)   !declaration for remaining atoms list

10      format(a9,1x,i3)
20      format(a16,1x,i3)
30      format(i3,5x,i3)
40      format(i4)
50	format(a19,1x,i4)
60	format(a8,1x,i4)
70	format(a13,1x,i4)

        open(unit=4,file='out.out',status='old')
        open(unit=2,file='le.out',status='unknown')
        read(4,10)str,inrot !str=NROTBONDS,inrot=number of rotatable bonds
	 ntor=inrot !ntor = number of torsions = number of rotatable bonds
c       write(*,10)str,inrot
c	print *,str,inrot
        do i=1,inrot
          read(4,30)(irotb(i,j),j=1,2)!irotb(i,1)=bonding atom no,irotb(i,2)=bond atom no
          read(4,20)str,el !str = Num_array_elment,el=0 
          iatrot(i) = el
          tem(i)=natom-iatrot(i)
          print *,'pdbgen,atrot',iatrot(i)
          print *,'pdbgen,tem',tem(i)        
              do k=1,iatrot(i)
                 read(4,40)ilsrot(i,k)
c                ire(i,k)=ilsrot(i,k)
	         ind(i,ilsrot(i,k))=1
                enddo
        enddo
c	print *,'cont'
	write(2,50)'number of rot_bonds',inrot
	do i=1,inrot
	write(2,60)'Rot_Bond',i
	write(2,70)'No.of Element',tem(i)
        write(*,*)'Rot_Bond',i
        write(*,*)'No. of Element',tem(i)
	 do k=1,natom
	    if (ind(i,k).ne.1) then
	      write(2,40),k
	    endif
          enddo
        enddo
	close(unit=2)

c	This is to list the atoms that are not displaced from its position

	open(unit=3,file='le.out',status='old')
c	open(unit=7,file='rele.out',status='unknown')

	read(3,50)str,inrot
c	write(7,40)inrot
	do i=1,inrot
	read(3,60)str,bonum(i)
c	write(7,40)bonum(i)
	read(3,70)str,ielenum(i)
c	write(7,40)ielenum(i)
		do j=1,ielenum(i)
		read(3,40)le(i,j)
c		write(7,40)le(i,j)
		enddo
	enddo
	close(unit=3)
c	close(unit=7)

	return
        end

c	*******************************************************************
	subroutine coorgen
	include 'mols.par'
c	program to extract the coordinates from the generated mol2. 
c	parameter (maxi=1000)

        common/crdb/y(maxatm,8)
        common /par/ natom, ntor, nhb, ns, lres
        common /getrot/inrot,irotb(maxi,2),el,ilsrot(maxi,maxi),
     &   iatrot(maxi),rx(100,3),ire(maxi,maxi),ind(maxi,maxi),big

     	common /left/ le(maxi,maxi),innd(maxi),ielenum(maxi),
     &	bonum(maxi)

	common /gen/ ibno,iat1(maxi),iat2(maxi),isno(maxi)
	common /string/ atsym(maxi),desc(maxi),attyp(maxi),
     &	str1(maxi),str2(maxi),str3,res
	


	character str*80
	character*50 desc*30,attyp*10,atsym*6,str1*30,str2*30,
     &	str3*30,res*10
	integer atno
	real r,big
c	dimension x(100,3)
10	format(a80)
11	format(a25)
20	format(i3,i3,a8)
c30	format(16x,3f10.4)
30      format(3x,i4,1x,a6,2x,3f10.4,1x,a29)
40	format(3f8.3)
50	format(1x,i5,1x,i5,1x,i5,a10)
	open(unit=1,file='out.mol2',status='old')
	
	do i=1,2
	read(1,11)str1(i)
c	write(*,11)str1(i)
	enddo

	read(1,20)atno,ibno,res
c	write(*,20)atno,ibno,res

	natom=atno
	do i=1,5
	read(1,11)str2(i)
c	write(*,11)str2(i)
	enddo

	do i=1,atno
        read(1,30),innd(i),atsym(i),(rx(i,j),j=1,3),desc(i)
c	write(*,30),innd(i),atsym(i),(rx(i,j),j=1,3),desc(i)
	y(i,j)=rx(i,j)
	enddo
	read(1,11)str3
c       write(*,11)str3
        do j=1,ibno
        read(1,50)isno(j),iat1(j),iat2(j),attyp(j)
c        write(*,50)isno(j),iat1(j),iat2(j),attyp(j)
        enddo
	big=0
        do k=1,natom
          do l=k+1,natom
           d=dist(rx(l,1),rx(l,2),rx(l,3),rx(k,1),rx(k,2),rx(k,3))
c         print*,l,k,d
            if(d .ge. big) then
             big=d
            endif
         enddo
        enddo
c       print*,'big=',big
c	stop

	close(unit=1)
	return
	end
c****************************************************************
	subroutine elemen(x1,y1,z1,x2,y2,z2,el,em,en)

c*****SUBROUTINE TO CALCULATE DIRECTION COSINES*****
c	write(*,*)'subroutine elemen - done'
	d = dist(x1,y1,z1,x2,y2,z2)
	   el=(x2-x1)/d
	   em=(y2-y1)/d
	   en=(z2-z1)/d
	return
	end

c***********************************************************************
	subroutine rotor(xr1,yr1,zr1,theta1,xr3,yr3,zr3,xr4,yr4,zr4)

c	write(31,*)'subroutine rotor - done'
c******SUBROUTINE TO ROTATE A POINT ABOUT A GENERAL AXIS BY THETA*****

            xp=xr3
	    yp=yr3
	    zp=zr3
	theta=theta1*3.1415927/180.0
c********* ROTATION MATRIX *********************************************
                    em11=cos(theta)+xr1*xr1*(1.0-cos(theta))
                    em12=xr1*yr1*(1.0-cos(theta))-zr1*sin(theta)
                    em13=xr1*zr1*(1.0-cos(theta))+yr1*sin(theta)
                    em21=xr1*yr1*(1.0-cos(theta))+zr1*sin(theta)
		    em22=cos(theta)+yr1*yr1*(1.0-cos(theta))
		    em23=yr1*zr1*(1.0-cos(theta))-xr1*sin(theta)
		    em31=xr1*zr1*(1.0-cos(theta))-yr1*sin(theta)
		    em32=yr1*zr1*(1.0-cos(theta))+xr1*sin(theta)
		    em33=cos(theta)+zr1*zr1*(1.0-cos(theta))
c************************************************************************
		xt=em11*xp+em12*yp+em13*zp
	        yt=em21*xp+em22*yp+em23*zp
		zt=em31*xp+em32*yp+em33*zp
	    xr4=xt
	    yr4=yt
	    zr4=zt
	return
	end

c*********************************************************************
	function dist(x1,y1,z1,x2,y2,z2)
	  dist = sqrt((x1-x2)**2+(y1-y2)**2+(z1-z2)**2)
	return
	end
c*********************************************************************
c-----------------------------receptor--------------------------------
	subroutine rcoorgen
        include 'mols.par'
c       program to extract the coordinates from the generated mol2.
c       parameter (maxi=1000)

        common/crdb/y(maxatm,8)
        common /par/ natom, ntor, nhb, ns, lres
        common /getrot/inrot,irotb(maxi,2),el,ilsrot(maxi,maxi),
     &   iatrot(maxi),rx(100,3),ire(maxi,maxi),ind(maxi,maxi),big

        common /left/ le(maxi,maxi),innd(maxi),ielenum(maxi),
     &  bonum(maxi)

        common /gen/ ibno,iat1(maxi),iat2(maxi),isno(maxi)
        common /string/ atsym(maxi),desc(maxi),attyp(maxi),
     &  str1(maxi),str2(maxi),str3,ress



        character str*80
        character*50 desc*30,attyp*10,atsym*6,str1*30,str2*30,
     &  str3*30,ress*10
        integer atno
        real r,big
c       dimension x(100,3)
10      format(a80)
11      format(a25)
20      format(i3,i3,a8)
c30     format(16x,3f10.4)
30      format(3x,i4,1x,a6,2x,3f10.4,1x,a29)
40      format(3f8.3)
50      format(1x,i5,1x,i5,1x,i5,a10)


  	open(unit=1,file='flexres.mol2',status='old')

        do i=1,2
        read(1,11)str1(i)
c       write(*,11)str1(i)
        enddo

        read(1,20)atno,ibno,ress
c       write(*,20)atno,ibno,ress

        natom=atno
        do i=1,5
        read(1,11)str2(i)
c       write(*,11)str2(i)
        enddo

        do i=1,atno
        read(1,30),innd(i),atsym(i),(rx(i,j),j=1,3),desc(i)
c       write(*,30),innd(i),atsym(i),(rx(i,j),j=1,3),desc(i)
        y(i,j)=rx(i,j)
        enddo
        read(1,11)str3
c       write(*,11)str3
        do j=1,ibno
        read(1,50)isno(j),iat1(j),iat2(j),attyp(j)
c        write(*,50)isno(j),iat1(j),iat2(j),attyp(j)
        enddo
        big=0

        do k=1,natom
          do l=k+1,natom
           d=dist(rx(l,1),rx(l,2),rx(l,3),rx(k,1),rx(k,2),rx(k,3))
c         print*,l,k,d
            if(d .ge. big) then
             big=d
            endif
         enddo
        enddo
c       print*,'big=',big
c       stop

	close(unit=1)
        return
        end
c------------------------------------------------------------------
        subroutine rstore_index
        include 'mols.par'
c       program to store the indices of the rotatable bonds and its neighbours
c       parameter (maxi=1000)

        common/crdb/y(maxatm,8)
        common /par/ natom, ntor, nhb, ns, lres
        common /getrot/inrot,irotb(maxi,2),el,ilsrot(maxi,maxi),
     &   iatrot(maxi),rx(100,3),ire(maxi,maxi),ind(maxi,maxi),big,
     &   irotbb(maxi,2)
        common /left/ le(maxi,maxi),innd(maxi),ielenum(maxi),
     &  bonum(maxi)

        character str*80
        integer el,tem(35)
c       integer numrot,bonum(50)   !declaration for remaining atoms list

10      format(a9,1x,i3)
20      format(a16,1x,i3)
30      format(i3,5x,i3)
40      format(i4)
50      format(a19,1x,i4)
60      format(a8,1x,i4)
70      format(a13,1x,i4)

        open(unit=4,file='flexres.out',status='old')
        open(unit=2,file='rle.out',status='unknown')
        open(unit=5,file='rbond.out',status='unknown')
        read(4,10)str,inrot !str=NROTBONDS,inrot=number of rotatable bonds
        ntor=inrot !ntor = number of torsions = number of rotatable bonds
c       write(*,10)str,inrot
        do i=1,inrot
          read(4,30)(irotb(i,j),j=1,2)!irotb(i,1)=bonding atom no,irotb(i,2)=bond atom no
c           write(5,30)(irotb(i,j),j=1,2)
c            endif
          read(4,20)str,el !str = Num_array_elment,el=0
          iatrot(i) = el
          
          tem(i)=natom-iatrot(i)
c          print *,'atrot',iatrot(i)
                do k=1,iatrot(i)
                 read(4,40)ilsrot(i,k)
c                ire(i,k)=ilsrot(i,k)
                 ind(i,ilsrot(i,k))=1
                enddo
        enddo
c----remove rotatable bonds involving N and C' --------
        do i=1,inrot
         if(irotb(i,1).ne.1)then
          if(irotb(i,1).ne.2)then
           write(5,30)(irotb(i,j),j=1,2)
c          read(5,30)(irotbb(i,j),j=1,2)
          endif
         endif
        enddo
c------------------------------------------------------

c       print *,'cont'
        write(2,50)'number of rot_bonds',inrot
        do i=1,inrot
        write(2,60)'Rot_Bond',i
        write(2,70)'No.of Element',tem(i)
         do k=1,natom
            if (ind(i,k).ne.1) then
              write(2,40),k
            endif
          enddo
        enddo
        close(unit=2)
        close(unit=5)

c       This is to list the atoms that are not displaced from its position

        open(unit=3,file='rle.out',status='old')
c       open(unit=7,file='rrele.out',status='unknown')

        read(3,50)str,inrot
c       write(7,40)inrot
        do i=1,inrot
        read(3,60)str,bonum(i)
c       write(7,40)bonum(i)
        read(3,70)str,ielenum(i)
c       write(7,40)ielenum(i)
                do j=1,ielenum(i)
                read(3,40)le(i,j)
c               write(7,40)le(i,j)
                enddo
        enddo
        close(unit=3)
c       close(unit=7)
c       print *,'store_index o.k'
        return
        end
c-----------------------------------------------------------------------------










