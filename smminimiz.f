c     library name : smminimiz.f   

c     Copyright (c) 2013 P.Arun Prasad, S.Nehru Viji and N.Gautham

c     This library is free software; you can redistribute it and/or
c     modify it under the terms of the GNU Lesser General Public
c     License as published by the Free Software Foundation; either
c     version 2.1 of the License, or (at your option) any later version.
c
c     This library is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
c     Lesser General Public License for more details.
c
c     You should have received a copy of the GNU Lesser General Public
c     License along with this library; if not, write to the Free
c     Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
c     02110-1301  USA
c
c
c     contact : n_gautham@hotmail.com  


        subroutine minimiz(inp)
cc	program minimize
	include 'mols.par'
       common /string1/str21(maxi),str22(maxi),str23,res2,atsym2(maxi),
     &  adesc2(maxi),attyp2(maxi)
        common /plp/ pat(mnatp), lat(maxatm),patnam(mnatp),
     & presnam(mnatp),pchid(mnatp),
     &  tpres(25), tatnam(25,50), natp,
     & tatyp(25,50), ntatp(25), px(mnatp,3)
        common /fnam/ if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4
        common /left/ le(maxi,maxi),innd(maxi),ielenum(maxi),
     &  bonum(maxi)

        common /gen/ ibno,iat1(maxi),iat2(maxi),isno(maxi)
        common /string/ atsym(maxi),desc(maxi),attyp(maxi),
     &  str1(maxi),str2(maxi),str3,res
        character str*80
        character*50 desc*30,attyp*10,atsym*6,str1*30,str2*30,
     &  str3*30,res*10,rmscr*35
        integer atno




        character*80  miniscr,flush
	character*80  str11,str12,str13,res1,atsym1,adesc1
	character*80  str21,str22,str23,res2,atsym2,adesc2
	character*80  attyp1,attyp2
        character*128 if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4,of0

	real rdihed,rvdw,reel,etot,rmmffene
	integer iatno1,ibno1,iatno2,ibno2,innd1(maxi),isno1(maxi),
     &  iat11(maxi),iat12(maxi),innd2(maxi),iat21(maxi),iat22(maxi),
     &  isno2(maxi),k11
        real molene,energy,rx1(100,3),rx2(100,3)
        miniscr='sh minimize.sh'

        call system(miniscr)
        open(unit=113,file="output2",status= "old")

        read(113,*)rdihed
        read(113,*)rvdw
        read(113,*)reel

c        print*,rdihed,rvdw,reel
        etot= rdihed+rvdw+reel
c        print*,etot
        close(unit=113)

        rmmffene = etot
	k11=inp
        rmscr='rm output2'
        call system(rmscr)

cv        open(unit=121,file='ene1.txt',status='old')
        open(unit=122,file='rplp.txt',status='old')
cv        read(121,*)molene
        read(122,*)plpe
	close(unit=113)
	close(unit=122)
        energy = plpe+rmmffene
        write(*,*)'generated minimized Structure NO : ',k11,energy
        write(31,*)'generated minimized Structure NO : ',k11,energy
c        open(unit=1,file='out.mol2',status='old')
        open(unit=2,file='min.mol2',status='old')
        open(unit=3,file='min1.mol2',status='unknown')
	open(unit=50,file=pf3,access='append')
11      format(a25)
12	  format(a17)
13	  format(a13)
21      format(a5,4x,i4,3x,f10.2)
22      format(a6)
23      format(a3)

c        write(50,21)'MODEL',k11,energy

        do i=1,2
        read(2,11)str21(i)
c        write(*,11)str21(i)
        write(3,12)str1(i)
        write(50,12)str1(i)
        enddo
20      format(i3,i3,a8)
        read(2,20)iatno2,ibno2,res2
c        write(*,20)iatno2,ibno2,res2
        write(3,20)atno,ibno,res
        write(50,20)iatno2,ibno2,res2
         do i=1,5
        read(2,11)str22(i)
c        write(*,11)str22(i)
        write(3,13)str22(i)
        write(50,13)str22(i)
        enddo
30      format(3x,i4,1x,a6,2x,3f10.4,1x,a29)
40      format(3f8.3)
50      format(1x,i5,1x,i5,1x,i5,a10)
        do i=1,iatno2
        read(2,30),innd2(i),atsym2(i),(rx2(i,j),j=1,3),adesc2(i)
c       write(*,30),innd2(i),atsym2(i),(rx2(i,j),j=1,3),adesc2(i)
       write(3,30),innd(i),atsym(i),(rx2(i,j),j=1,3),adesc2(i)
       write(50,30),innd(i),atsym(i),(rx2(i,j),j=1,3),adesc2(i)
        enddo
c        read(1,11)str13
        read(2,11)str23
       write(3,11)str23
       write(50,11)str23
        do j=1,ibno2
c        read(1,50)isno1(j),iat11(j),iat12(j),attyp1(j)
        read(2,50)isno2(j),iat21(j),iat22(j),attyp2(j)
        write(3,50)isno2(j),iat21(j),iat22(j),attyp2(j)
        write(50,50)isno2(j),iat21(j),iat22(j),attyp2(j)
        enddo
	write(50,23)'TER'
        write(50,22)'ENDMDL'

	
        close(unit=1)
        close(unit=2)
        close(unit=3)
        close(unit=50)
        end

c********************************************************************
