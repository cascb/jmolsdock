        program cpyres
        include 'mols.par'

c       parameter (maxm=20000)
        common /par/ natom, ntor, nhb, ns, lres
        common /tor/ u0(maxpar),sn(maxpar),tn(maxpar),phi(maxpar)
        common /ctl/iopt,iff,icint,fseq(maxres),fopt
        common /cen/bx,by,bz
        common /fnam/ if1,pf1,pf2,pf3,pf4,pf5,of1,of2,of3,of4
        common/mout/opmo(maxstr,maxpar),opmi(maxstr,maxpar),
     &              emo(maxstr),emi(maxstr)
        common /dcom/ipatom,pfile,path,mname
c       common /pcom/rfile,ipatom
        character mname*128,path*128
        character pfile*128,mol*128,str*80
        real bx,by,bz
        integer is,ie,it
c       integer ipatom
        
        
        open(unit=30,file='rec_tem.inp',status='old')
c        read(30,'(A)') path
c        print *,'path name :  ',path
c        read(30,*) mname
c        print *,'molecule name :  ',mname
c        read(30,'(A)')mol
c        read(30,*)it
c        read(30,*) is,ie
c        read(30,*)bx,by,bz
        read(30,'(A)') pfile
        print *,pfile
        close(30)
        
        call iread_pdb
        call cpy_pdb        

        end
c-----------------------------------------------------------
        subroutine iread_pdb
        parameter (maxm=20000)
        common /dcom/ipatom,pfile,path,mname
c       common /pcom/ipatom,rfile
        character pfile*128,str*80
        
10      format(a80)
        open(unit=1,file=pfile,status='old')
        do i=1,maxm
        read(1,10,end=99),str
        enddo
99      write(*,*),'line number',i

        ipatom=i-2
        write(*,*)ipatom
c100    stop
        close(unit=1)
        return
        end
c-----------------------------------------------------------
        subroutine cpy_pdb
        include 'mols.par'
        common /dcom/ipatom,pfile
c       common /pcom/rfile,ipatom

        character str*80,pfile*128
10      format(a80)
        open(unit=1,file=pfile,status='unknown')
        open(unit=2,file='receptor.pdb',status='unknown')
        do i=1,ipatom
        read(1,10)str
        write(2,10)str
        enddo
        close(2)
        close(1)
        return
        end

c-----------------------------------------------------------

