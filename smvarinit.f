c     library name : smvarinit.f   

c     Copyright (c) 2013 P.Arun Prasad, S.Nehru Viji and N.Gautham

c     This library is free software; you can redistribute it and/or
c     modify it under the terms of the GNU Lesser General Public
c     License as published by the Free Software Foundation; either
c     version 2.1 of the License, or (at your option) any later version.
c
c     This library is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
c     Lesser General Public License for more details.
c
c     You should have received a copy of the GNU Lesser General Public
c     License along with this library; if not, write to the Free
c     Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
c     02110-1301  USA
c
c
c     contact : n_gautham@hotmail.com  

        subroutine varinit
	include 'mols.par'
	common /par/ natom, ntor, nhb, ns, lres
	common /fnam/ if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4
	common /tor/ u0(maxpar),sn(maxpar),tn(maxpar),phi(maxpar)
        common /atm/ iatmno(maxatm),atname(maxatm),rename(maxatm),
     &              iresno(maxatm)
	common /var/ ivat(maxres*8,4),vty(maxres*8),ivres(maxres*8)
        common/order/nn,mm
        common/vectors/iv(maxpar,4)
        common/crda/x(maxatm,8) 
        common/crdb/y(maxatm,8) 
	common/pdbat/atom(maxatm),ele(maxatm)
        common/ranges/jstart(maxatm,10),jend(maxatm,10),j1_4(maxatm,25)
        common/hb/ihb1(maxhb),ihb2(maxhb),c(maxhb),d(maxhb)
       	common/ctl/iopt,iff,icint,fseq(maxres)
	common /plp/ pat(mnatp), attyp(maxatm),patnam(mnatp),
     & presnam(mnatp),pchid(mnatp),
     &  tpres(25), tatnam(25,50), natp,
     & tatyp(25,50), ntatp(25), px(mnatp,3)
        common /getrot/inrot,irotb(maxi,2),el,ilsrot(maxi,maxi),
     &   iatrot(maxi),rx(100,3),ire(maxi,maxi),ind(maxi,maxi),big
        common /left/ le(maxi,maxi),innd(maxi),ielenum(maxi),
     &  bonum(maxi)
        common /gen/ ibno,iat1(maxi),iat2(maxi),isno(maxi)
	 common /cen/bx,by,bz

        character*50 desc*30,attyp*10,atsym*6,str1*30,str2*30,
     &  str3*30,res*10


        dimension x_one(maxatm,3),x_two(maxatm,3)
  
  	external dihedr
  	integer iff
	character*128 if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4
	character*30 atom*30, ele*24
	character dd1*4,dd2*4,dd3*4,dd4*4,dd5*4,vty*4,tcode*4,scode*1,
     &  atname*4, rename*4,fseq*1
     	character xatnam*4, xresnam*3,lat*2,pat*2,tatyp*2
	character patnam*4,presnam*3,tpres*3,tatnam*4
	nn = ntor
41	format(i5,2x,4i5,2x,a4)
112	format(8x,4a4,2x,a4,3f7.2)
90	format(a4,a1,1x,i2)
10	format(8x,i3,1x,a4,1x,a4,3x,i3)
c
 	print *,nn	
cv	open(unit=1,file=pf1,status='old')
cv	do i=1,natom
cv		read(1,10) iatmno(i),atname(i),rename(i),iresno(i)
c		write(*,*) iatmno(i),atname(i),rename(i),iresno(i)
cv	enddo
cv	close(unit=1)
cv	call pdb
cv	call input
c-----------------define dihedral angles atoms-------------------------
cc	open(unit=21,file='molsx.inp',status='unknown')
cv	nvar = 0
cv	do i = 1,lres
cv	rewind 10
cv	open(unit=10,file='VAR.lib',status='old')
cv	do j=1,111
cv	read(10,90) tcode,scode,natm
ccv	print *,tcode,scode,natm,iopt,fseq(i)
cv	if(scode.eq.fseq(i))then
cv	if(iopt.eq.1) natm = 2
cv	do j1=1,natm
cv	idd1= 0
cv	idd2= 0
cv	idd3= 0
cv	idd4= 0
cv	tu0 = 0.0
cv	tsn = 0.0
cv	ttn = 0.0
cv	read(10,112) dd1,dd2,dd3,dd4,dd5,tu0,tsn,ttn
ccv	print *,dd1,dd2,dd3,dd4,dd5,tu0,tsn,ttn
cv	if(i.eq.1.and.dd5.eq.'phi ')goto 251
cv	if(i.eq.lres.and.dd5.eq.'psi ')goto 251
cv	do j2=1,natom
cv	if(dd5.eq.'phi ') then
cv	if(dd1.eq.atname(j2).and.iresno(j2).eq.(i-1)) idd1=j2
cv	if(dd2.eq.atname(j2).and.iresno(j2).eq.i) idd2=j2
cv	if(dd3.eq.atname(j2).and.iresno(j2).eq.i) idd3=j2
cv	if(dd4.eq.atname(j2).and.iresno(j2).eq.i) idd4=j2
cv	elseif(dd5.eq.'psi ')then
cv	if(dd1.eq.atname(j2).and.iresno(j2).eq.i) idd1=j2
cv	if(dd2.eq.atname(j2).and.iresno(j2).eq.i) idd2=j2
cv	if(dd3.eq.atname(j2).and.iresno(j2).eq.i) idd3=j2
cv	if(dd4.eq.atname(j2).and.iresno(j2).eq.(i+1)) idd4=j2
cv	else
cv	if(dd1.eq.atname(j2).and.iresno(j2).eq.i) idd1=j2
cv	if(dd2.eq.atname(j2).and.iresno(j2).eq.i) idd2=j2
cv	if(dd3.eq.atname(j2).and.iresno(j2).eq.i) idd3=j2
cv	if(dd4.eq.atname(j2).and.iresno(j2).eq.i) idd4=j2
cv	endif
cv	if(idd1.ne.0.and.idd2.ne.0.and.idd3.ne.0.and.idd4.ne.0)goto 251
cv	enddo
cv251	nvar = nvar + 1
ccv	write(*,41) i,idd1, idd2, idd3, idd4,dd5
cccv	write(21,41) i, idd1, idd2, idd3, idd4, dd5
cv	ivres(nvar) = i
cv	vty(nvar) = dd5
cv	ivat(nvar,1) = idd1
cv	ivat(nvar,2) = idd2
cv	ivat(nvar,3) = idd3
cv	ivat(nvar,4) = idd4
cv	u0(nvar) = tu0
cv	sn(nvar) = tsn
cv	tn(nvar) = ttn
cv	enddo
cv	endif
cv	enddo
cv	enddo	
cv	close(unit=10)
cccv	close(unit=21)
c----------measure the dihedral angles of initial model -----------------
cv	do i=1,nn
cv	dih = 0.0
cv	  i1 = ivat(i,1)
cv	  i2 = ivat(i,2)
cv	  i3 = ivat(i,3)
cv	  i4 = ivat(i,4)
cv	if(i1.ne.0.and.i2.ne.0.and.i3.ne.0.and.i3.ne.0) then
cv	dih = dihedr(i1,i2,i3,i4)
cv	endif
cv	phi(i) = dih
ccv	  print *,i,i1,i2,i3,i4,dih
cv	enddo

c	write(31,*)(phi(ii),ii=1,nn)
c	write(*,*)(phi(ii),ii=1,nn)

	do ii = 1,nn
c	write(31,*)ii,phi(ii)
	phi(ii) = (-1.0*phi(ii))+180.0
c	write(31,*)ii,phi(ii)
	enddo
ccccc
c	do ii = 1,nn
c	phi(ii) = 0.0
c	enddo
cccc
c
c	write(31,*)'dihedral angles section - done '
c	write(*,*)(phi(ii),ii=1,nn)
        do k=1,natom !natom = number of ligand atoms
           do ki=1,3
             x_one(k,ki)=rx(k,ki)!rx(k,ki) are the x,y,z co-ordinates of the ligand from out.mol2 
           enddo
        enddo
        do if=1,nn
	

C###################### PHI ALL ####################################      
	!irotb(1,1) has the value of the atom with rotatable bond::refer smpdbgen.f
	!x_one(irotb(1,1),1) is the 'x' co-ordinate of the first atom of rotatable bond 
	!x_one(irotb(1,1),2) is the 'y' co-ordinate of the first atom of rotatable bond
	!x_one(irotb(1,1),3) is the 'z' co-ordinate of the first atom of  rotatable bond 
        call elemen(x_one(irotb(if,1),1),x_one(irotb(if,1),2),
     &              x_one(irotb(if,1),3),
     &              x_one(irotb(if,2),1),x_one(irotb(if,2),2),
     &              x_one(irotb(if,2),3),
     &              el,em,en)
50      format(i4,1x,3f8.3)
        do j=1,ielenum(if)
        k=le(if,j)
           do ki=1,3
           x_two(k,ki)=x_one(k,ki) !x,y,z co-ordinates of out.mol2 copied to x_two(k,ki)
c       write(*,50)k,x_two(k,ki)
c	write(31,*)k,' ',x_two(k,ki)
	enddo
        enddo

c       write(*,50)k,x_two(k,ki)
c******************************************************************

c        do k=1,iv(if,3)-1
c           do ki=1,3
c             x_two(k,ki)=x_one(k,ki) 
c           enddo
c        enddo
c******************************************************************
         
	do j=1,iatrot(if)
c        write(31,*)j
	  k=ilsrot(if,j)
c	  write(31,*)'smvarinit - iatrot section - done'!this section was not printed -so this section is not executed
c         write(*,*)
           xin=x_one(k,1)-x_one(irotb(if,1),1)
           yin=x_one(k,2)-x_one(irotb(if,1),2)
           zin=x_one(k,3)-x_one(irotb(if,1),3)
          call rotor(el,em,en,phi(if),xin,yin,zin,
     &                xout,yout,zout)
c        print*,'phi',el,em,en,phi(if)
c           write(*,*)'xoout',xout,yout,zout
           x_two(k,1)=xout+x_one(irotb(if,1),1)
           x_two(k,2)=yout+x_one(irotb(if,1),2)
           x_two(k,3)=zout+x_one(irotb(if,1),3)


c       print*,'rot_coord'
c        write(21,50),k,x_two(k,1),x_two(k,2),x_two(k,3)
        enddo

        do k=1,natom
           do ki=1,3
              x_one(k,ki)=x_two(k,ki)
           enddo
        enddo
C##################################################################
        enddo
c	write(31,*)'check smvarinit - done' !this section is getting executed
        do k=1,natom
           do ki=1,3
             x(k,ki)=x_two(k,ki) 
             y(k,ki)=x_two(k,ki) 
           enddo
        enddo
c
c15	format(a30,3f8.3,a24)
c	open(unit=50,file='molsx.pdb',status='unknown')
c	do i=1,natom
c	write (50,15) atom(i),y(i,1),y(i,2),y(i,3),ele(i)
c	enddo
c	close(unit=50)
c	stop
	call dockinit
        return
        end

c********************************************************************
	subroutine pdb
	include 'mols.par'
        common/crda/x(maxatm,8) 
        common /par/ natom, ntor, nhb, ns, lres
	common/pdbat/atom(maxatm),ele(maxatm)
	common /fnam/ if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4
	character*128 if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4,of0
	character*30 atom
        character*24 ele
	open(unit=26,file=pf1,status='old')
c	open(unit=26,file='molso.pdb',status='old')
c	open(unit=26,file='scheraga.pdb',status='old')
15	format(a30,3f8.3,a24)
	do i=1,natom
        read(26,15)atom(i),(x(i,j),j=1,3),ele(i)
c        read(26,15)atom(i),xt,yt,zt,ele(i)
	enddo
	close(unit=26)
	return
	end
c***********************************************************************
        subroutine input

	include 'mols.par'
        common/crda/x(maxatm,8) 
        common/ranges/jstart(maxatm,10),jend(maxatm,10),j1_4(maxatm,25)
        common /par/ natom, ntor, nhb, ns, lres
        common/vectors/iv(maxpar,4)
        common/order/nn,mm
        common/hb/ihb1(maxhb),ihb2(maxhb),c(maxhb),d(maxhb)
       	common/ctl/iopt,iff,icint,fseq(maxres)
	common /fnam/ if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4
	character*128 if1,pf1,pf2,pf3,pf4,of1,of2,of3,of4,of0
      	character fseq*1


        open(unit=1,file=if1,status='old')
c        open(unit=1,file='gly.inp',status='old')

        do i=1,natom
          read(1,*) atn,(x(i,j),j=4,8)
          i1=ifix(x(i,7))
          i2=ifix(x(i,8))
          read(1,*)(jstart(i,j),jend(i,j),j=1,i1),(j1_4(i,k),k=1,i2)
        enddo

        do i=1,nn
          read(1,*)(iv(i,j),j=1,4)
        enddo
	if(iff.eq.1) then
        do i=1,nhb
          read(1,*)ihb1(i),ihb2(i),c(i),d(i)
        enddo
	endif
        close(unit=1)

       	return
        end
c*******************************************************************
c************************************************************************
       function dihedr(i1,i2,i3,i4)!this function is not used in MOLSDOCK
	include 'mols.par'
        common/crda/x(maxatm,8)
        acdc=((180.0*7.0)/22.0)
	one=1.d0
      x1=x(i2,1)-x(i1,1)
      y1=x(i2,2)-x(i1,2)
      z1=x(i2,3)-x(i1,3)
      x2=x(i3,1)-x(i2,1)
      y2=x(i3,2)-x(i2,2)
      z2=x(i3,3)-x(i2,3)
      ux1=y1*z2-z1*y2
      uy1=z1*x2-x1*z2
      uz1=x1*y2-y1*x2
      x1=x(i4,1)-x(i3,1)
      y1=x(i4,2)-x(i3,2)
      z1=x(i4,3)-x(i3,3)
      ux2=z1*y2-y1*z2
      uy2=x1*z2-z1*x2
      uz2=y1*x2-x1*y2

      u1=ux1*ux1+uy1*uy1+uz1*uz1
      u2=ux2*ux2+uy2*uy2+uz2*uz2
      u=u1*u2
c     write(31,*)'dihedr - done'
      if (u.ne.zero) then
        a=(ux1*ux2+uy1*uy2+uz1*uz2)/sqrt(u)
        a=max(a,-one)
        a=min(a,one)
        dihedr=acos(a)*acdc
        if (ux1*(uy2*z2-uz2*y2)+uy1*(uz2*x2-ux2*z2)+
     #      uz1*(ux2*y2-uy2*x2).lt.zero) dihedr =-dihedr
        return
      else
        write (*,'(a,4i5)')' dihedr> Error in coordinates of atoms #: '
     #                     ,i1,i2,i3,i4
        stop
      endif
      end
c****************************************************************************
	subroutine dockinit()
	include 'mols.par'
	common/pdbat/atom(maxatm),ele(maxatm)
	common /par/ natom, ntor, nhb, ns, lres
        common /plp/ pat(mnatp), lat(maxatm),patnam(mnatp),
     &  presnam(mnatp),pchid(mnatp),
     &  tpres(25), tatnam(25,50), natp,
     &  tatyp(25,50), ntatp(25), px(mnatp,3)
        common /getrot/inrot,irotb(maxi,2),el,ilsrot(maxi,maxi),
     &   iatrot(maxi),rx(100,3),ire(maxi,maxi),ind(maxi,maxi),big
 
        common /strin/ tem1(maxi),tem2(maxi),slig_str3(maxi),res,
     &	ato(maxi)
	 common /cen/bx,by,bz
        common /dcom/ipatom,pfile,path,mname

    	real x1,x2,x3,d,a,b,c,y,ene,cx,cy,cz
        character yy4*4,zz3*3,z3*3,ltyp*4,ptyp*4,xx*1,xx4*4,xx3*3,
     & xatnam*4, xresnam*3,lat*2,pat*2,tatyp*2,yy5*4
	character patnam*4,presnam*3,tpres*3,tatnam*4,atom*30
	character resnum*4
        character*80 tem1*30,tem2*30,slig_str3*30,res*10,ato*80
        character pfile*128,path*128,mname*128
	real cutdist
	print *,'Read recepor file, assign atom type to receptor'
	print *,'atoms and ligand atoms'

10	format(12x,a4,1x,a3,1x,a1,8x,3f8.3)
11	format(12x,a4,1x,a3,10x,3f8.3,23x,a2)

c***********read atom records of protein and ligand******************
c	natp =2195
c	Enter no. of atoms with/without water molecule
c	natp = 4412
	
	natp=ipatom

	cx = 0.0
	cy = 0.0
	cz = 0.0
	cutdist = 0.0
c	print*,'bx=',bx,'by=',by,'bz=',bz
c        bx= -9.665
c        by= 16.179
c        bz= 1.860


ccc	open(unit=1,file='~/vdock/dset/1blh.pdb',
ccc     &	status='unknown')


	open(unit=1,file=pfile,status='unknown')
c	open(unit=2,file='inhibitor.pdb', status='unknown')
	ic = 0
	do i=1,natp
        read(1,10) xatnam,xresnam,xchid,x1,x2,x3
	if(xatnam(1:1).eq.'H')goto 100
	if(xatnam(2:2).eq.'H')goto 100
	cutdist =dist(bx,by,bz,x1,x2,x3)
c	print *,'cutdist: ',cutdist
	if(cutdist.gt.(5.0+6.0+big))goto 100
	ic = ic+1
	patnam(ic)=xatnam
	presnam(ic)=xresnam
	pchid(ic)=xchid
	px(ic,1) = x1
	px(ic,2) = x2
	px(ic,3) = x3
c        read(1,10) patnam(i),presnam(i),pchid(i),(px(i,j),j=1,3)
c	print *,ic
c	write(6,10)patnam(ic),presnam(ic),pchid(ic),(px(ic,j),j=1,3)
100	enddo

c	print *,'no. of patoms:',ic
	natp = ic

c	do i=1,natl
c       read(2,11) latnam(i),lresnam(i),(lx(i,j),j=1,3),lat(i)
c        write(6,11) latnam(i),lresnam(i),(lx(i,j),j=1,3),lat(i)
c	enddo
	close(unit=1)
c	close(unit=2)
c	stop
c***********Read template file for protein atom types*************
	open(unit=3,file='PATOMTYPE', status='unknown')
12	format(5x,a3,2x,i2)
13	format(5x,a4,17x,a2)
14	format(a1)
15 	format(f8.3,2x,f8.3,2x,f8.3)
16	format(a2)
	do i = 1,25
	read(3,12) tpres(i),ntatp(i)
c	write(6,12) tpres(i),ntatp(i)
	do j = 1,ntatp(i)
	read(3,13) tatnam(i,j),tatyp(i,j)
c	write(6,13) tatnam(i,j),tatyp(i,j)
	enddo
	read(3,14) xx
	enddo
c	stop
c***********Assign atom type for protein ******************
	print *,'Protein atom and its type'
	do i = 1,natp
	xx4 = patnam(i)
	xx3 = presnam(i)
	do j1 = 1,25
	zz3 = tpres(j1)
	if(zz3.eq.xx3) then
	do j2 = 1,ntatp(j1)
	yy4 = tatnam(j1,j2)
	yy5 = tpres(j1)
	if(yy4(1:2).eq.xx4(2:3).and.yy5(1:3).eq.xx3(1:3))then
	pat(i) = tatyp(j1,j2)
c	print *,xx4, ' ', xx3
c	write(6,16) pat(i)

c	exit
	endif
	enddo
	endif
	enddo
	enddo
c	stop
c***********Assign atom type for ligand ******************
	print *,'Ligand atom and its type'
	print *,'no. of lig atoms: ',natom
c	do i=1,natom
c	xx4 = atom(i)(13:16)
c	xx3 = atom(i)(18:20)
c	k = 0
c	do j1 = 1,25
c	zz3 = tpres(j1)
c	if(zz3.eq.xx3) then
c	do j2 = 1,ntatp(j1)
c	yy4 = tatnam(j1,j2)
c	if(yy4(1:2).eq.xx4(2:3)) then
c	lat(i) = tatyp(j1,j2)
c	print *,xx4, ' ', xx3
c	write(6,16) lat(i)
c	exit
c	k = 1
c	endif
c	enddo
c	endif
c	enddo
c	if(k.eq.0) lat(i) = 'X'
c	enddo
	print *,natp

c	do i=1,natom
c	xx4 = atom(i)(13:16)
c	xx3 = atom(i)(18:20)
c	print *,xx4,' ',xx3,' ',lat(i)
c	enddo
c	stop
!*********************************************************************
22      format(a80)
33      format(a25)
44      format(i3,i3,a8)
55      format(a30)

        open(unit=7,file='tem.mol2',status='old')
        open(unit=8,file='atomtype.dat',status='old')

        do l=1,31
        read(8,55),slig_str3(l)
c       write(*,55),slig_str3(l)
        enddo

        do i=1,2
        read(7,33)tem1(i)
c        write(*,33)tem1(i)
        enddo

        read(7,44)natom,ibno,res
c        write(*,44)natom,ibno,res

        do j=1,5
        read(7,33)tem2(j)
c        write(*,33)tem2(j)
        enddo

        do  k=1,natom
        read(7,22),ato(k)
c       write(*,22),ato(k)
          do  n=1,31
          if( ato(k)(48:55) .eq. slig_str3(n)(6:13) ) then
          lat(k)= slig_str3(n)(23:24)
c	print*,lat(k)
c       write(6,40),k,ato(k)(48:55),str3(n)(6:13),str3(n)(23:24)
           endif
40      format(1x,i3,a10,1x,a10,1x,a3)
           enddo
        enddo
c       do m=1,natom
c       lat(m)=attyp(m)
c       write(*,*)m,attyp(m)
c       enddo

        close(7)
        close(8)

	return
        end
!***********************************************************************

